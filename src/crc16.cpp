/*********************************************************************************************

 EazyLink2 - Fast Client/Server Z88 File Management
 Copyright (C) 2012-2015 Gunther Strube (gstrube@gmail.com) & Oscar Ernohazy

 EazyLink2 is free software; you can redistribute it and/or modify it under the terms of the
 GNU General Public License as published by the Free Software Foundation;
 either version 2, or (at your option) any later version.
 EazyLink2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.
 You should have received a copy of the GNU General Public License along with EazyLink2;
 see the file COPYING. If not, write to the
 Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

**********************************************************************************************/

#include "crc16.h"

Crc16::Crc16()
{
    resetChecksum();
}

Crc16::Crc16(quint16 initValue) {
    crc16Val = initValue;
}

/**
  * Initialize CRC-16 to 0xffff
  */
void Crc16::resetChecksum() {
    crc16Val = 0xffff;
}


/**
  * Initialize CRC-16 to specified value
  */
void Crc16::resetChecksum(quint16 initValue) {
    crc16Val = initValue;
}


/**
  * Obtain the current CRC-16 as calculated by latest use of checksum()
  * or initial CRC-16 as specified by constructor or resetChecksum().
  *
  * @returns current CRC-16
  */
quint16 Crc16::getChecksum() {
    return crc16Val;
}


/**
  * @brief Calculate CRC-16 of entire specified data block, using current CRC-16 as initial value.
  *
  * Test cases:
  * initial 0x0000, "123456789" generates 0x31C3 CRC-CCITT (XModem)
  * initial 0xFFFF, "123456789" generates 0x29B1 CRC-CCITT (0xFFFF)
  *
  * @param s is the data block to be CRC-16 iterated.
  * @return calculate new CRC-16, of specified data block
  */
quint16 Crc16::checksum(const QByteArray &s) {
    return checksum(s, 0, s.size());
}

quint16 Crc16::checksum(const QByteArray &s, int offset, int len)
{
    quint16 x;

    for (int i = offset;  i < (offset+len);  i++) {
        x = crc16Val >> 8 ^ (s[i] & 0xff);
        x ^= x>>4;
        crc16Val = (crc16Val << 8) ^ ((quint16)(x << 12)) ^ ((quint16)(x <<5)) ^ ((quint16)x);
    }

    return crc16Val;
}
