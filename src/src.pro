# *********************************************************************************************
#
# EazyLink2 - Fast Client/Server Z88 File Management
# Copyright (C) 2012-2015 Gunther Strube (gstrube@gmail.com) & Oscar Ernohazy
#
# EazyLink2 is free software; you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation;
# either version 2, or (at your option) any later version.
# EazyLink2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with EazyLink2;
# see the file COPYING. If not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# **********************************************************************************************/

QT += core gui serialport widgets

TARGET = eazylink2
TEMPLATE = app
ICON = Eazylink.icns
CONFIG += ordered warn_on qt debug_and_release

DESTDIR = ../bin
MOC_DIR = ../build/moc
RCC_DIR = ../build/rcc
UI_DIR = ../build/ui
unix:OBJECTS_DIR = ../build/o/unix
win32:OBJECTS_DIR = ../build/o/win32
macx:OBJECTS_DIR = ../build/o/mac

SOURCES += main.cpp\
    mainwindow.cpp\
    crc16.cpp \
    crc32.cpp \
    z88serialport.cpp \
    z88_devview.cpp \
    z88storageviewer.cpp \
    z88filespec.cpp \
    desktop_view.cpp \
    serialportsavail.cpp \
    prefrences_dlg.cpp \
    actionsettings.cpp \
    setupwizard.cpp \
    setupwiz_intro.cpp \
    setupwiz_modeselect.cpp \
    setupwiz_serialselect.cpp \
    setupwiz_finalpage.cpp \
    z88comms.cpp \
    console.cpp \
    protocoltest.cpp \
    z88filedate.cpp

HEADERS += mainwindow.h\
    crc16.h \
    crc32.h \
    z88serialport.h \
    serialportsavail.h \
    z88_devview.h \
    z88storageviewer.h \
    z88filespec.h \
    desktop_view.h \
    prefrences_dlg.h \
    actionsettings.h \
    setupwizard.h \
    setupwiz_intro.h \
    setupwiz_modeselect.h \
    setupwiz_serialselect.h \
    setupwiz_finalpage.h \
    z88comms.h \
    console.h \
    protocoltest.h \
    z88filedate.h

FORMS += mainwindow.ui \
    prefrences_dlg.ui \
    actionsettings.ui \
    setupwiz_intro.ui \
    setupwiz_modeselect.ui \
    setupwiz_serialselect.ui \
    setupwiz_finalpage.ui \
    protocoltest.ui

RC_FILE = eazylink2.rc

RESOURCES += \
    images.qrc \
    fonts.qrc

