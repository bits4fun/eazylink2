/*********************************************************************************************

 EazyLink2 - Fast Client/Server Z88 File Management
 Copyright (C) 2012-2015 Gunther Strube (gstrube@gmail.com) & Oscar Ernohazy

 EazyLink2 is free software; you can redistribute it and/or modify it under the terms of the
 GNU General Public License as published by the Free Software Foundation;
 either version 2, or (at your option) any later version.
 EazyLink2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.
 You should have received a copy of the GNU General Public License along with EazyLink2;
 see the file COPYING. If not, write to the
 Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

**********************************************************************************************/

#include "setupwizard.h"
#include "setupwiz_intro.h"
#include "ui_setupwiz_intro.h"

/**
  * Setup Wizard Intro Page
  */
SetupWiz_Intro::SetupWiz_Intro(QWidget *parent) :
    QWizardPage(parent),
    ui(new Ui::SetupWiz_Intro),
    m_scene(this)
{

    QFont font("vera");
    font.setStyleHint(QFont::SansSerif);
    font.setStyle(QFont::StyleNormal);
    font.setPixelSize(11);
    setFont(font);
    ui->setupUi(this);

    /**
      * Add the Picture of Z88 linked to Host
      */
    m_scene.addPixmap(QPixmap(":/images/z88Intro"));

    static const QString Welcome_msg = "Welcome to the Eazylink2 Client Setup Wizard. The next\n"
                                       "few pages will guide you through configuring the client\n"
                                       "so it can start communicating with the Z88. Please make \n"
                                       "sure the Z88 has fresh batteries, and is connected to an\n"
                                       "available serial port on your host computer.\n";

    ui->IntroPic->setScene(&m_scene);
    ui->IntroText->setText(Welcome_msg);
}

SetupWiz_Intro::~SetupWiz_Intro()
{
    delete ui;
}

