﻿/*********************************************************************************************

 EazyLink2 - Fast Client/Server Z88 File Management, Z88 EazyLink Communication Protocol
 Copyright (C) 2011-2015 Gunther Strube (gstrube@gmail.com)

 EazyLink2 is free software; you can redistribute it and/or modify it under the terms of the
 GNU General Public License as published by the Free Software Foundation;
 either version 2, or (at your option) any later version.
 EazyLink2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.
 You should have received a copy of the GNU General Public License along with EazyLink2;
 see the file COPYING. If not, write to the
 Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

**********************************************************************************************/

#include <QtCore/QCoreApplication>
#include <QtCore/QEventLoop>
#include <QtCore/QList>
#include <QtCore/QDateTime>
#include <QtCore/QTime>
#include <QtCore/QFile>
#include <QtCore/QByteArray>
#include <QElapsedTimer>
#include "z88serialport.h"
#include "crc16.h"


QT_USE_NAMESPACE

Z88SerialPort::Z88SerialPort()
{
    eazylinkProtocolLevel = 1; // set default (should be higher when EazyLink popdown is polled)
    eazylinkVersion = "";

    char _synchEazyLinkProtocol[] = { 1, 1, 2 };
    char _escEsc[] = { 27, 27 };
    char _esc1b[] = { 27, 'B', '1', 'B' };
    char _escB[] = { 27, 'B' };
    char _escN[] = { 27, 'N' };
    char _escE[] = { 27, 'E' };
    char _escF[] = { 27, 'F' };
    char _escY[] = { 27, 'Y' };
    char _escZ[] = { 27, 'Z' };
    char _escCrcStart[] = { 27, '[' };
    char _escCrcEnd[] = { 27, ']' };
    char _helloCmd[] = { 27, 'a' };
    char _quitCmd[] = { 27, 'q' };
    char _devicesCmd[] = { 27, 'h' };
    char _directoriesCmd[] = { 27, 'd' };
    char _filesCmd[] = { 27, 'n' };
    char _transOnCmd[] = { 27, 't' };
    char _transOffCmd[] = { 27, 'T' };
    char _lfConvOnCmd[] =  { 27, 'c' };
    char _lfConvOffCmd[] = { 27, 'C' };
    char _reloadTraTableCmd[] = { 27, 'z' };
    char _versionCmd[] =  { 27, 'v' };
    char _ramDefaultCmd[] = { 27, 'g' };
    char _fileExistCmd[] = { 27, 'f' };
    char _fileDateStampCmd[] = { 27, 'u' };
    char _setFileTimeStampCmd[] = { 27, 'U' };
    char _fileSizeCmd[]= { 27, 'x' };
    char _getZ88TimeCmd[] = { 27, 'e' };
    char _setZ88TimeCmd[] = { 27, 'p' };
    char _createDirCmd[] = { 27, 'y' };
    char _deleteFileDirCmd[] = { 27, 'r' };
    char _renameFileDirCmd[] = { 27, 'w' };
    char _freeMemoryCmd[] = { 27, 'm' };
    char _freeMemDevCmd[] = { 27, 'M' };
    char _receiveFilesCmd[] = { 27, 's' };
    char _sendFilesCmd[] = { 27, 'b' };
    char _crc32FileCmd[] = { 27, 'i' };
    char _devInfoCmd[] = { 27, 'O' };
    char _changeSpeedCmd[] = { 27, 'L' };
    char _useXonXoffCmd[] = { 27, 'X' };
    char _useRtsCtsCmd[] = { 27, 'Y' };

    // Initialize ESC command constants
    synchEazyLinkProtocol = QByteArray( _synchEazyLinkProtocol, 3); // EazyLink Synchronize protocol
    escEsc = QByteArray( _escEsc, 2);
    esc1b = QByteArray( _esc1b, 4);
    escB = QByteArray( _escB, 2);
    escN = QByteArray( _escN, 2);
    escE = QByteArray( _escE, 2);
    escF = QByteArray( _escF, 2);
    escY = QByteArray( _escY, 2);
    escZ = QByteArray( _escZ, 2);
    escCrcStart = QByteArray( _escCrcStart, 2);                 // EazyLink V5.4 CRC-16 Data Block Begin
    escCrcEnd = QByteArray( _escCrcEnd, 2);                     // EazyLink V5.4 CRC-16 Data Block End
    helloCmd = QByteArray( _helloCmd , 2);                      // EazyLink V4.4 Hello
    quitCmd = QByteArray( _quitCmd, 2);                         // EazyLink V4.4 Quit
    devicesCmd = QByteArray( _devicesCmd, 2);                   // EazyLink V4.4 Device
    directoriesCmd = QByteArray( _directoriesCmd, 2);           // EazyLink V4.4 Directory
    filesCmd = QByteArray( _filesCmd, 2);                       // EazyLink V4.4 Files
    receiveFilesCmd = QByteArray( _receiveFilesCmd, 2);         // EazyLink V4.4 Receive one or more files from Z88
    sendFilesCmd = QByteArray( _sendFilesCmd, 2);               // EazyLink V4.4 Send one or more files from Z88
    transOnCmd = QByteArray( _transOnCmd , 2);                  // EazyLink V4.4 Translation ON
    transOffCmd = QByteArray( _transOffCmd, 2);                 // EazyLink V4.4 Translation OFF
    lfConvOnCmd = QByteArray( _lfConvOnCmd, 2);                 // EazyLink V4.4 Linefeed Conversion ON
    lfConvOffCmd = QByteArray( _lfConvOffCmd, 2);               // EazyLink V4.4 Linefeed Conversion OFF
    versionCmd = QByteArray( _versionCmd, 2);                   // EazyLink V4.5 EazyLink Server Version and protocol level
    fileExistCmd = QByteArray( _fileExistCmd, 2);               // EazyLink V4.5 File exists
    fileDateStampCmd = QByteArray( _fileDateStampCmd, 2);       // EazyLink V4.5 Get create and update date stamp of Z88 file
    setFileTimeStampCmd = QByteArray( _setFileTimeStampCmd, 2); // EazyLink V4.5 Set create and update date stamp of Z88 file
    fileSizeCmd = QByteArray( _fileSizeCmd, 2);                 // EazyLink V4.5 Get file size
    reloadTraTableCmd = QByteArray( _reloadTraTableCmd, 2);     // EazyLink V4.6 Reload Translation Table
    deleteFileDirCmd = QByteArray( _deleteFileDirCmd, 2);       // EazyLink V4.6 Delete file/dir on Z88.
    ramDefaultCmd = QByteArray( _ramDefaultCmd, 2);             // EazyLink V4.7 Get Z88 RAM defaults
    createDirCmd = QByteArray( _createDirCmd, 2);               // EazyLink V4.7 Create directory path on Z88
    renameFileDirCmd = QByteArray( _renameFileDirCmd, 2);       // EazyLink V4.7 Rename file/directory on Z88
    getZ88TimeCmd = QByteArray( _getZ88TimeCmd, 2);             // EazyLink V4.8 Get Z88 system Date/Time
    setZ88TimeCmd = QByteArray( _setZ88TimeCmd, 2);             // EazyLink V4.8 Set Z88 System Clock using PC system time
    freeMemoryCmd = QByteArray( _freeMemoryCmd, 2);             // EazyLink V4.8 Get free memory for all RAM cards
    freeMemDevCmd = QByteArray( _freeMemDevCmd, 2);             // EazyLink V5.0 Get free memory for specific device
    crc32FileCmd = QByteArray( _crc32FileCmd, 2);               // EazyLink V5.2 Get CRC-32 of specified Z88 file
    devInfoCmd = QByteArray( _devInfoCmd, 2);                   // EazyLink V5.2 Get Device Information
    changeSpeedCmd = QByteArray( _changeSpeedCmd, 2);           // EazyLink V5.3 Change to 38400 bps
    useXonXoffCmd = QByteArray( _useXonXoffCmd, 2);             // EazyLink V5.3 Switch to Xon/Xoff software handshaking protocol
    useRtsCtsCmd = QByteArray( _useRtsCtsCmd, 2);               // EazyLink V5.3 Switch to Rts/Cts hardware handshaking protocol

    transmitting = portOpenStatus = z88AvailableStatus = false;
    port = new QSerialPort(this);

    connect(port, SIGNAL(error(QSerialPort::SerialPortError)), this,
            SLOT(handleError(QSerialPort::SerialPortError)));

    connect(port, SIGNAL(bytesWritten(qint64)), this, SLOT(handleBytesWritten(qint64)));
    connect(port, SIGNAL(readyRead()), this, SLOT(handleReadyRead()));

}

Z88SerialPort::~Z88SerialPort()
{
    if (portOpenStatus == true) {
        port->close();
    }

    delete port;
}


QSerialPortInfo *Z88SerialPort::getSerialPortDeviceInfo()
{
    if (portOpenStatus == true) {
        return portInfo;
    } else {
        return NULL;
    }
}


// notification of serial input port error, SIGNAL(error(QSerialPort::SerialPortError))
void Z88SerialPort::handleError(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::ResourceError) {
        qDebug() << "Z88SerialPort::handleError():" << port->errorString();
    }
}

/**
 * @brief event that is triggered by QtSerialport when bytes have been successfully sent
 * @param bytes
 */
void Z88SerialPort::handleBytesWritten(qint64 bytes)
{
    totalBytesSent += bytes;

    //qDebug() << "handleBytesWritten(" << bytes << "):" << totalBytesSent << "(" << totalBytesToSend << ") bytes sent.";
    if (totalBytesToSend == totalBytesSent) {
        //qDebug() << "Completed.";
        bytesSent = true;
    }
}


/**
 * @brief event that is triggered by QtSerialport when bytes are ready to be fetched from serial port
 */
void Z88SerialPort::handleReadyRead()
{
    // qDebug() << "time since last byte received:" << timeSinceLastReceivedByte.elapsed();
    timeSinceLastReceivedByte.restart();

    response.append(port->readAll());
    emit totalBytesRead(response.length());
}


/**
 * @brief returns the time (in ms) taken to flush latest file data successfully to serial port buffer
 * @return qint64
 */
qint64 Z88SerialPort::getFileBufferFlushTime() const
{
    return fileBufferFlushTime;
}


/**
 * @brief returns total bytes of latest file data stream sent successfully
 * @return int
 */
int Z88SerialPort::getTotalFileTransferStream() const
{
    return totalFileTransferStream;
}


QString Z88SerialPort::getErrorString()
{
    if (portOpenStatus == true) {
        return port->errorString();
    } else {
        return "Serial port is not open!";
    }

}

/**
 * Open serial port to Z88 with default configuration
 * (9600, No Parity, 8 data bits, one stop bit, Rts/Cts hardware handshaking)
 *
 * @return true if port was opened successfully otherwise false
 */
bool Z88SerialPort::openSerialPort()
{
    return openSerialPort(portName, 9600, true); // use default serial port device name (or previously specified port name), using default 9600 bps, Xon/Xoff
}


/**
 * Open serial port with specified name and speed, using hardware handshaking
 * (No Parity, 8 data bits, one stop bit)
 * For Z88/EazyLink, 9600 and 38400 is supported
 *
 * @brief Open serial port with specified name and speed, using hardware handshaking
 * @param pName
 * @param speed (9600 or 38400)
 * @return true if port was opened successfully, otherwise false
 */
bool Z88SerialPort::openSerialPort(QString pName, qint32 speed, bool hardwareFlowControl)
{
    if (portOpenStatus == true) {
        // close connection if currently open...
        port->close();
        delete(portInfo);
        portInfo = NULL;
        portOpenStatus = false;
    }

    portInfo = new QSerialPortInfo(pName);
    portName = portInfo->systemLocation();
    port->setPortName(portName);
#if QT_VERSION >= 0x050300
    port->setBaudRate(speed);
    port->setParity(QSerialPort::NoParity);
    port->setDataBits(QSerialPort::Data8);
    port->setStopBits(QSerialPort::OneStop);

    if (hardwareFlowControl == true)
        port->setFlowControl(QSerialPort::HardwareControl);
    else
        port->setFlowControl(QSerialPort::SoftwareControl);
#endif

    if (!port->open(QIODevice::ReadWrite)) {
        qDebug() << QObject::tr("Failed to open port %1, error: %2").arg(pName).arg(port->errorString()) << Qt::endl;
        portOpenStatus = false;
    } else {
        portOpenStatus = true;
    }

#if QT_VERSION < 0x050300
    port->setBaudRate(speed);
    port->setParity(QSerialPort::NoParity);
    port->setDataBits(QSerialPort::Data8);
    port->setStopBits(QSerialPort::OneStop);

    if (hardwareFlowControl == true)
        port->setFlowControl(QSerialPort::HardwareControl);
    else
        port->setFlowControl(QSerialPort::SoftwareControl);
#endif

    return portOpenStatus;
}


void Z88SerialPort::setPortName(QString pName)
{
    portName = pName;
}


QString Z88SerialPort::getPortName()
{
    return portName;
}


int Z88SerialPort::getSerialPortSpeed()
{
    if (isOpen()) {
        return port->baudRate();
    } else {
        return -1;
    }
}

bool Z88SerialPort::isOpen()
{
    return portOpenStatus;
}


bool Z88SerialPort::isZ88Available()
{
    return z88AvailableStatus;
}


bool Z88SerialPort::isTransmitting()
{
    return transmitting;
}


void Z88SerialPort::closeSerialport()
{
    if (portOpenStatus == true) {
        // close connection if currently open...
        port->close();
        portOpenStatus = false;
    }

    // set default for ezylink popdown (unknown)
    eazylinkVersion = "";
    eazylinkProtocolLevel = 1;

    z88AvailableStatus = false;
}


/**
 * @brief Get information of the current release version of EazyLink popdown
 * @return
 */
QString Z88SerialPort::getEazylinkPopdownVersion()
{
    return eazylinkVersion;
}

/**
 * @brief Get information of the current protocol level of EazyLink popdown
 * @return
 */
int Z88SerialPort::getEazylinkPopdownProtocolLevel()
{
    return eazylinkProtocolLevel;
}


/*****************************************************************************
 *      EazyLink Server V4.4
 *      Send a 'hello' to the Z88 and return true if Yes, or false if No
 *****************************************************************************/
bool Z88SerialPort::helloZ88()
{
    if ( sendCommand(helloCmd, 1000) == true) {
        if (transmitting == true) {
            qDebug() << "helloZ88(): Transmission already ongoing with Z88 - aborting...";
        } else {
            waitforResponse(1000, escY); // give the Z88 max 1s to respond
            transmitting = false;

            if ( response.count() != 2) {
                qDebug() << "helloZ88(): Bad response from Z88!";
            } else {
                if (response.at(1) == 'Y') {
                    //qDebug() << "helloZ88(): Z88 responded 'hello'!";

                    // wait 1 second before continuing (let Eazylink V5.2 or earlier popdown switch to rts/cts handshake serial protocol)...
                    wait(1000);

                    // cache version & protocol level immediate for internal usage
                    getEazyLinkZ88Version();

                    return true;        // hello response correctly received from Z88
                }
            }
        }
    }

    return false;
}


/*****************************************************************************
 *      EazyLink Server V4.4
 *      Send a 'quit' to the Z88 and return true if Z88 responded
 *      This command tries to return within 1s
 *****************************************************************************/
bool Z88SerialPort::quitZ88()
{
    if (portOpenStatus == true) {
        if (transmitting == false) {
            if (sendBytes(synchEazyLinkProtocol) == true) {

                waitforResponse(500, synchEazyLinkProtocol); // wait max 0.5s for synch response
                if ( response.count() == 3) {
                    if (response.at(2) == 2) {
                        if ( sendBytes(quitCmd) == true) {
                            waitforResponse(500, escY); // give the Z88 max 0.5s to respond
                            transmitting = false;

                            if ( response.count() == 2) {
                                if (response.at(1) == 'Y') {
                                    z88AvailableStatus = false;
                                    return true;        // quit response correctly received from Z88
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return false;
}


/*****************************************************************************
 *      EazyLink Server V5.3
 *      Switch to Rts/Cts hardware handshaking, returns true if Z88 responded
 *****************************************************************************/
bool Z88SerialPort::useRtsCts()
{
    if ( sendCommand(useRtsCtsCmd,2000) == true) {
        if (transmitting == true) {
            qDebug() << "useRtsCts(): Transmission already ongoing with Z88 - aborting...";
        } else {
            waitforResponse(1000, escY); // give the Z88 max 1s to respond
            transmitting = false;

            if ( response.count() != 2) {
                qDebug() << "useRtsCts(): Bad response from Z88!";
            } else {
                if (response.at(1) == 'Y') {
                    //qDebug() << "useRtsCts(): Z88 responded 'yes'!";
                    return true;        // rts/cts response correctly received from Z88
                }
            }
        }
    }

    return false;
}


/*****************************************************************************
 *      EazyLink Server V5.3
 *      Switch to Xon/Xoff software handshaking, returns true if Z88 responded
 *****************************************************************************/
bool Z88SerialPort::useXonXoff()
{
    if ( sendCommand(useXonXoffCmd, 2000) == true) {
        if (transmitting == true) {
            qDebug() << "useXonXoff(): Transmission already ongoing with Z88 - aborting...";
        } else {
            waitforResponse(1000, escY); // give the Z88 max 1s to respond
            transmitting = false;

            if ( response.count() != 2) {
                qDebug() << "useXonXoff(): Bad response from Z88!";
            } else {
                if (response.at(1) == 'Y') {
                    //qDebug() << "useXonXoff(): Z88 responded 'yes'!";
                    return true;        // xon/xoff response correctly received from Z88
                }
            }
        }
    }

    return false;
}


/*****************************************************************************
 *      EazyLink Server V5.3
 *      Switch to 38.400 BPS, returns true if Z88 responded
 *****************************************************************************/
bool Z88SerialPort::use38400bps()
{
    if ( sendCommand(changeSpeedCmd, 3000) == true) {
        if (transmitting == true) {
            qDebug() << "use38400bps(): Transmission already ongoing with Z88 - aborting...";
        } else {
            waitforResponse(1000, escY); // give the Z88 max 1s to respond
            transmitting = false;

            if ( response.count() != 2) {
                qDebug() << "use38400bps(): Bad response from Z88!";
            } else {
                if (response.at(1) == 'Y') {
                    //qDebug() << "use38400bps(): Z88 responded 'yes'!";
                    return true;        // response correctly received from Z88
                }
            }
        }
    }

    return false;
}


/*****************************************************************************
 *      EazyLink Server V4.4
 *      Get Z88 devices.
 *****************************************************************************/
QList<QByteArray> Z88SerialPort::getDevices()
{
    QList<QByteArray> deviceList;

    if ( sendCommand(devicesCmd,1500) == true) {
        if (transmitting == true) {
            qDebug() << "getDevices(): Transmission already ongoing with Z88 - aborting...";
        } else {
            // receive device elements into list
            receiveListItems(deviceList,1500);
            transmitting = false;
        }
    }

    return deviceList;
}


/*****************************************************************************
 *      EazyLink Server V4.4
 *      Get directories in defined path, directories are returned in list
 *****************************************************************************/
QList<QByteArray> Z88SerialPort::getDirectories(const QString &path, bool &retc)
{
    QList<QByteArray> directoriesList;

    QByteArray directoriesCmdPath = directoriesCmd;
    directoriesCmdPath.append(path.toUtf8());
    directoriesCmdPath.append(escZ);

    if ( sendCommand(directoriesCmdPath,3000) == true) {
        if (transmitting == true) {
            qDebug() << "getDirectories(): Transmission already ongoing with Z88 - aborting...";
        } else {
            // receive device elements into list
            retc = receiveListItems(directoriesList,5000);
            transmitting = false;
        }
    }

    return directoriesList;
}


/*****************************************************************************
 *      EazyLink Server V4.4
 *      Get filename in defined path, filenames are returned in list
 *****************************************************************************/
QList<QByteArray> Z88SerialPort::getFilenames(const QString &path, bool &retc)
{
    QList<QByteArray> filenamesList;

    QByteArray filesCmdPath = filesCmd;
    filesCmdPath.append(path.toUtf8());
    filesCmdPath.append(escZ);

    if ( sendCommand(filesCmdPath,5000) == true) {
        if (transmitting == true) {
            qDebug() << "getFilenames(): Transmission already ongoing with Z88 - aborting...";
            retc = false;
        } else {
            // receive device elements into list
            retc = receiveListItems(filenamesList,5000);
            transmitting = false;
        }
    }

    return filenamesList;
}


/*****************************************************************************
 *      EazyLink Server V4.4
 *      Enable translation mode during file transfer
 *****************************************************************************/
bool Z88SerialPort::translationOn()
{
    if ( sendCommand(transOnCmd,500) == true) {
        return true;  // Z88 now has translations enabled
    } else {
        return false;
    }
}


/*****************************************************************************
 *      EazyLink Server V4.4
 *      Disable translation mode during file transfer
 *****************************************************************************/
bool Z88SerialPort::translationOff()
{
    if ( sendCommand(transOffCmd,500) == true) {
        return true;  // Z88 now has translations disabled
    } else {
        return false;
    }
}


/*****************************************************************************
 *      EazyLink Server V4.4
 *      Enable linefeed conversion mode during file transfer
 *****************************************************************************/
bool Z88SerialPort::linefeedConvOn()
{
    if ( sendCommand(lfConvOnCmd,500) == true) {
        return true;  // Z88 now has linefeed conversion enabled
    } else {
        return false;
    }
}


/*****************************************************************************
 *      EazyLink Server V4.4
 *      Disable linefeed conversion mode during file transfer
 *****************************************************************************/
bool Z88SerialPort::linefeedConvOff()
{
    if ( sendCommand(lfConvOffCmd,500) == true) {
        return true;  // Z88 now has linefeed conversion disabled
    } else {
        return false;
    }
}


/*****************************************************************************
 *      EazyLink Server V4.6
 *      Remote updating of translation table
 *      (reload and install translation table from Z88 filing system)
 *****************************************************************************/
bool Z88SerialPort::reloadTranslationTable()
{
    if ( sendCommand(reloadTraTableCmd,500) == true) {
        return true;  // Z88 now has reload translation table.
    } else {
        return false;
    }
}


/*****************************************************************************
 *      EazyLink Server V4.5
 *      Get EazyLink Server (main) Version and protocol level
 *****************************************************************************/
QByteArray Z88SerialPort::getEazyLinkZ88Version()
{
    QByteArray popdownVersion;
    QList<QByteArray> versionList;

    if ( sendCommand(versionCmd,1000) == true) {
        if (transmitting == true) {
            qDebug() << "getEazyLinkZ88Version(): Transmission already ongoing with Z88 - aborting...";
        } else {
            // receive version string
            receiveListItems(versionList,1000);
            transmitting = false;

            if (versionList.count() > 0) {
                popdownVersion.clear();

                popdownVersion = versionList.at(0); // the "list" has only one item...
                if (popdownVersion.length() > 0) {
                    // preserve a copy EazyLink popdown version & protocol for internal management
                    // of communication protocol
                    QString vs = QString(popdownVersion);
                    eazylinkProtocolLevel = vs.split("-")[1].toInt(); // convert 2nd part of splitted string into int...
                    eazylinkVersion = vs.split("-")[0];
                }
            }
        }
    }

    return popdownVersion;
}


/*****************************************************************************
 *      EazyLink Server V4.7
 *      Get Z88 RAM defaults
 *****************************************************************************/
QList<QByteArray> Z88SerialPort::getRamDefaults()
{
    QList<QByteArray> ramDefaultList;

    if ( sendCommand(ramDefaultCmd,1000) == true) {
        if (transmitting == true) {
            qDebug() << "getRamDefaults(): Transmission already ongoing with Z88 - aborting...";
        } else {
            // receive device elements into list
            receiveListItems(ramDefaultList,1000);
            transmitting = false;
        }
    }

    return ramDefaultList;
}


/*****************************************************************************
 *      EazyLink Server V4.5
 *      Respond ESC 'Y' if file exists on Z88 (return true)
 *****************************************************************************/
bool Z88SerialPort::isFileAvailable(const QString &filename)
{
    QByteArray fileExistCmdRequest = fileExistCmd;
    fileExistCmdRequest.append(filename.toUtf8());
    fileExistCmdRequest.append(escZ);

    if ( sendCommand(fileExistCmdRequest,1500) == true) {
        if (transmitting == true) {
            qDebug() << "isFileAvailable(): Transmission already ongoing with Z88 - aborting...";
        } else {
            waitforResponse(2000, escY); // give the Z88 max 2s to respond
            transmitting = false;

            if ( response.count() != 2) {
                qDebug() << "isFileAvailable(): Bad response from Z88!";
            } else {
                if (response.at(1) == 'Y') {
                    return true;        // Z88 responded that file exists
                }
            }
        }
    }

    return false; // no serial port communication or file not available..
}


/*****************************************************************************
 *      EazyLink Server V4.5
 *      Get create and update date stamp of Z88 file
 *****************************************************************************/
QList<QByteArray> Z88SerialPort::getFileDateStamps(const QString &filename)
{
    QList<QByteArray> dateStampList;

    QByteArray fileDateStampCmdRequest = fileDateStampCmd;
    fileDateStampCmdRequest.append(filename.toUtf8());
    fileDateStampCmdRequest.append(escZ);

    if ( sendCommand(fileDateStampCmdRequest,2000) == true) {
        if (transmitting == true) {
            qDebug() << "getFileDateStamps(): Transmission already ongoing with Z88 - aborting...";
        } else {
            // receive date stamps into list
            receiveListItems(dateStampList,3000);
            transmitting = false;
        }
    }

    return dateStampList;
}


/*****************************************************************************
 *      EazyLink Server V4.5
 *      Get size of Z88 file (in bytes)
 *****************************************************************************/
QByteArray Z88SerialPort::getFileSize(const QString &filename)
{
    QList<QByteArray> fileSizeList;
    QByteArray fileSizeString;

    QByteArray fileSizeCmdRequest = fileSizeCmd;
    fileSizeCmdRequest.append(filename.toUtf8());
    fileSizeCmdRequest.append(escZ);

    if ( sendCommand(fileSizeCmdRequest,2000) == true) {
        if (transmitting == true) {
            qDebug() << "getFileSize(): Transmission already ongoing with Z88 - aborting...";
        } else {
            // receive file size into list
            receiveListItems(fileSizeList,3000);
            transmitting = false;

            if (fileSizeList.count() > 0) {
                fileSizeString = fileSizeList.at(0); // the "list" has only one item...
            }
        }
    }

    return fileSizeString;
}


/*****************************************************************************
 *      EazyLink Server V4.8
 *      Get Z88 system Date/Time (Clock)
 *****************************************************************************/
QList<QByteArray> Z88SerialPort::getZ88Time()
{
    QList<QByteArray> timeList;

    if ( sendCommand(getZ88TimeCmd,1000) == true) {
        if (transmitting == true) {
            qDebug() << "getZ88Time(): Transmission already ongoing with Z88 - aborting...";
        } else {
            // receive time stamp into list
            receiveListItems(timeList,1000);
            transmitting = false;
        }
    }

    return timeList;
}


/*****************************************************************************
 *      Set Z88 system Date/Time, if time difference between
 *      Z88 and Desktop is bigger than 30 seconds
 *
 *      Returns true, if Desktop time has been synchronized to Z88
 *      Returns false, if time was not necessary to be set or there was a communication error
 *****************************************************************************/
bool Z88SerialPort::syncZ88Time()
{
    QList<QByteArray> z88TimeList = getZ88Time();

    if (z88TimeList.count()==2) {
        QDateTime z88Dt = QDateTime::fromString(
                    QString(z88TimeList[0].data()).append(z88TimeList[1].data()),
                    "dd/MM/yyyyhh:mm:ss");

        int timeDiff = QDateTime::currentDateTime().secsTo(z88Dt);
        timeDiff = ( timeDiff < 0 ? -timeDiff : timeDiff );

        if (timeDiff > 30)
            return setZ88Time();
    }

    return false;
}


/*****************************************************************************
 *      EazyLink Server V4.8
 *      Set Z88 System Clock using PC system time
 *****************************************************************************/
bool Z88SerialPort::setZ88Time()
{
    QDateTime dt = QDateTime::currentDateTime();

    QByteArray setZ88TimeCmdRequest = setZ88TimeCmd;
    setZ88TimeCmdRequest.append(dt.toString("dd/MM/yyyy").toUtf8());
    setZ88TimeCmdRequest.append(escN);
    setZ88TimeCmdRequest.append(dt.toString("hh:mm:ss").toUtf8());
    setZ88TimeCmdRequest.append(escZ);

    if ( sendCommand(setZ88TimeCmdRequest,2000) == true) {
        if (transmitting == true) {
            qDebug() << "setZ88Time(): Transmission already ongoing with Z88 - aborting...";
        } else {
            waitforResponse(5000, escY); // give the Z88 max 5s to process
            transmitting = false;

            if ( response.count() != 2) {
                qDebug() << "setZ88Time(): Bad response from Z88!";
            } else {
                if (response.at(1) == 'Y') {
                    return true;        // Z88 time was set
                }
            }
        }
    }

    return false;
}


/*****************************************************************************
 *      EazyLink Server V4.5
 *      Set create and update date stamp of Z88 file
 *****************************************************************************/
bool Z88SerialPort::setFileDateStamps(const QString &filename, QByteArray createDate, QByteArray updateDate)
{
    QByteArray setFileTimeStampCmdRequest = setFileTimeStampCmd;

    setFileTimeStampCmdRequest.append(filename.toUtf8());
    setFileTimeStampCmdRequest.append(escN);
    setFileTimeStampCmdRequest.append(createDate);
    setFileTimeStampCmdRequest.append(escN);
    setFileTimeStampCmdRequest.append(updateDate);
    setFileTimeStampCmdRequest.append(escZ);

    if ( sendCommand(setFileTimeStampCmdRequest,2000) == true) {
        if (transmitting == true) {
            qDebug() << "setZ88Time(): Transmission already ongoing with Z88 - aborting...";
        } else {
            waitforResponse(3000, escY); // give the Z88 max 2s to process
            transmitting = false;

            if ( response.count() != 2) {
                qDebug() << "setFileDateStamps(): Bad response from Z88!";
            } else {
                if (response.at(1) == 'Y') {
                    return true;        // time stamps were set
                }
            }
        }
    }

    return false;
}


/*****************************************************************************
 *      EazyLink Server V4.7
 *      Create directory path on Z88.
 *****************************************************************************/
bool Z88SerialPort::createDir(const QString &pathName)
{
    QByteArray createDirCmdRequest = createDirCmd;
    createDirCmdRequest.append(pathName.toUtf8());
    createDirCmdRequest.append(escZ);

    if ( sendCommand(createDirCmdRequest) == true) {
        if (transmitting == true) {
            qDebug() << "createDir(): Transmission already ongoing with Z88 - aborting...";
        } else {
            waitforResponse(4000, escY); // give the Z88 1s time to process
            transmitting = false;

            if ( response.count() != 2) {
                qDebug() << "createDir(): Bad response from Z88!";
            } else {
                if (response.at(1) == 'Y') {
                    return true;        // file/dir were created
                }
            }
        }
    }

    return false;
}


/*****************************************************************************
 *      EazyLink Server V4.6
 *      Delete file/dir on Z88.
 *****************************************************************************/
bool Z88SerialPort::deleteFileDir(const QString &filename)
{
    QByteArray deleteFileDirCmdRequest = deleteFileDirCmd;
    deleteFileDirCmdRequest.append(filename.toUtf8());
    deleteFileDirCmdRequest.append(escZ);

    if ( sendCommand(deleteFileDirCmdRequest,2000) == true) {
        if (transmitting == true) {
            qDebug() << "deleteFileDir(): Transmission already ongoing with Z88 - aborting...";
        } else {
            waitforResponse(60000, escY); // give the Z88 60s timeout to process (eg. for big files > 256K)
            transmitting = false;

            if ( response.count() != 2) {
                qDebug() << "deleteFileDir(): Bad response from Z88!";
            } else {
                if (response.at(1) == 'Y') {
                    return true;        // file/dir were deleted
                }
            }
        }
    }

    return false;
}


/*****************************************************************************
 *      EazyLink Server V4.7
 *      Rename file/directory on Z88.
 *****************************************************************************/
bool Z88SerialPort::renameFileDir(const QString &pathName, const QString &fileName)
{
    QByteArray renameFileDirCmdRequest = renameFileDirCmd;
    renameFileDirCmdRequest.append(pathName.toUtf8());  // filename (with explicit path)
    renameFileDirCmdRequest.append(escN);
    renameFileDirCmdRequest.append(fileName.toUtf8());  // short filename (12+3, without path)
    renameFileDirCmdRequest.append(escZ);

    if ( sendCommand(renameFileDirCmdRequest,1000) == true) {
        if (transmitting == true) {
            qDebug() << "renameFileDir(): Transmission already ongoing with Z88 - aborting...";
        } else {
            waitforResponse(2000, escY); // give the Z88 2s to process filename
            transmitting = false;

            if ( response.count() != 2) {
                qDebug() << "renameFileDir(): Bad response from Z88!";
            } else {
                if (response.at(1) == 'Y') {
                    return true;        // file/dir were renamed
                }
            }
        }
    }

    return false;
}


/*****************************************************************************
 *      EazyLink Server V4.8
 *      Get free memory for all RAM cards
 *****************************************************************************/
QByteArray Z88SerialPort::getZ88FreeMem()
{
    QList<QByteArray> freeMemoryList;
    QByteArray freeMemoryString;

    if ( sendCommand(freeMemoryCmd,1000) == true) {
        if (transmitting == true) {
            qDebug() << "getZ88FreeMem(): Transmission already ongoing with Z88 - aborting...";
        } else {
            // receive free memory string into list
            receiveListItems(freeMemoryList,3000);
            transmitting = false;

            if (freeMemoryList.count() > 0) {
                freeMemoryString = freeMemoryList.at(0); // the "list" has only one item...
            }
        }
    }

    return freeMemoryString;
}


/*****************************************************************************
 *      EazyLink Server V4.8
 *      Get free memory for specific device
 *****************************************************************************/
QByteArray Z88SerialPort::getZ88DeviceFreeMem(QByteArray device)
{
    QByteArray freeMemDevCmdRequest = freeMemDevCmd;
    QList<QByteArray> freeMemoryList;
    QByteArray freeMemoryString;

    freeMemDevCmdRequest.append(device);
    freeMemDevCmdRequest.append(escZ);

    if ( sendCommand(freeMemDevCmdRequest,1000) == true) {
        if (transmitting == true) {
            qDebug() << "getZ88DeviceFreeMem(): Transmission already ongoing with Z88 - aborting...";
        } else {
            // receive free memory string into list
            receiveListItems(freeMemoryList,3000);
            transmitting = false;

            if (freeMemoryList.count() > 0) {
                freeMemoryString = freeMemoryList.at(0); // the "list" has only one item...
            }
        }
    }

    return freeMemoryString;
}


/*****************************************************************************
 *      Prepare ImpExport popdown datastream for Z88 Imp/Export protocol (Imp/Export popdown batch mode)
 *      Caller must ensure that the filename applies to Z88 standard
 *      Returns complete filestream, to be sent using sendImpExpFileData(), or no empty stream (failure)
 *****************************************************************************/
QByteArray Z88SerialPort::prepareImpExpFile(const QString &z88Filename, const QString hostFilename)
{
    QByteArray fileContents;
    QFile hostFile(hostFilename);

    if (hostFile.exists() == true) {
        if (hostFile.open(QIODevice::ReadOnly) == true) {
            // qDebug() << "impExpSendFile(): Transmitting '" << hostFilename << "' file...";
            // file opened for read-only...
            QByteArray fileContents = hostFile.readAll();
            hostFile.close();

            return createImpExpFileData(z88Filename, fileContents);
        } else {
            qDebug() << "impExpSendFile(): Couldn't open File for reading - aborting...";
        }

    } else {
        qDebug() << "impExpSendFile(): File doesn't exist - aborting...";
    }

    return fileContents;
}


/*****************************************************************************
 *      Receive a file from Z88 using EazyLink protocol
 *      Received file will be stored at <hostPath>
 *****************************************************************************/
Z88SerialPort::retcode Z88SerialPort::receiveFile(const QString &z88Filename, const QString &hostPath, const QString &destFspec, bool destisDir)
{
    QByteArray receiveFileCmdRequest = receiveFilesCmd;
    QByteArray remoteFile;
    int b;

    if (transmitting == true) {
        qDebug() << "receiveFile(): Transmission already ongoing with Z88 - aborting...";
    } else {
        receiveFileCmdRequest.append(z88Filename.toUtf8());
        receiveFileCmdRequest.append(escZ);

        if ( sendCommand(receiveFileCmdRequest,2000) == true) {
            if (transmitting == true) {
                qDebug() << "receiveFile(): Transmission already ongoing with Z88 - aborting...";
                return rc_busy;
            } else {
                QString hostFilename = hostPath;

                if(destisDir){
                    hostFilename.append('/');
                    hostFilename.append(destFspec);
                }

                QFile hostFile(hostFilename);
                QString tfile = hostFilename + ".xfer";

                if (hostFile.exists() == true) {
                    // automatically replace existing host file
                    hostFile.remove();
                }

                //qDebug() << "receiveFile(): Receiving '" << z88Filename << "' to '" << hostFilename << "' file...";

                // wait max 5s if file reception aborts prematurely
                if (waitforResponse(5000, escZ) == true) {

                    remoteFile.clear();
                    int fileDataPos = response.indexOf(escF);
                    if (fileDataPos == -1) {
                        // ESC F marker not found in received data stream...
                        return rc_inv;
                    }

                    // build true file image by parsing received byte stream
                    for (b = fileDataPos+2; b<response.length(); b++) {
                        int byte = response[b];

                        switch(byte) {
                            case 27:
                                byte = response[++b];

                                switch(byte) {
                                    case 'B':
                                        // parse ESC B XX sequence
                                        byte = xtod(response[b+1])*16 + xtod(response[b+2]);
                                        b += 2;
                                        remoteFile.append(byte);
                                        break;

                                    case 'E':
                                        // ESC E is end of file
                                        if (hostFile.open(QIODevice::WriteOnly) == true) {
                                            hostFile.write(remoteFile);     // write entire collected remote file contents to host file
                                            hostFile.close();
                                            hostFile.rename(tfile);
                                            hostFile.rename(hostFilename);
                                            return rc_done;
                                        } else {
                                            qDebug() << "receiveFile(): Couldn't open File '" << hostFilename << "' for writing - aborting...";
                                            return rc_inv;
                                        }

                                    case 27:
                                        // ESC ESC means ESC (used in protocol level 1-8)
                                        remoteFile.append( (char) 27);
                                        break;

                                    default:
                                        // illegal escape command - skip data...
                                        return rc_inv;
                                }
                                break;

                            default:
                                remoteFile.append(byte);
                        }
                    }
                }
            }
        }
    }

    return rc_inv;
}


/*****************************************************************************
 *      Receive one or more files from Z88 using Imp/Export protocol (Imp/Export popdown batch mode)
 *      Received files will be stored at <hostPath>
 *****************************************************************************/
bool Z88SerialPort::impExpReceiveFiles(const QString &hostPath)
{
    bool fileWritten;
    int fileNamePos = 0, fileDataPos = 0, waitCounter = 10;
    QByteArray z88FileStream, z88Filename, remoteFile, hexBytes;

    do {
        // wait max 3s for End Batch from Imp/export or time out, then read data stream
        waitforResponse(3000, escZ);

        // get a copy of response...
        z88FileStream.append(response);

        if (response.length() == 0) {
            waitCounter--;
        } else
            break;
    } while (waitCounter);

    if (waitCounter == 0)
        // no data was received...
        return false;

    while(1) {
        remoteFile.clear();
        fileNamePos = z88FileStream.indexOf(escN, fileNamePos);
        if (fileNamePos == -1) {
            // ESC N marker not found in received data stream - no more files available...
            return true;
        } else {
            fileDataPos = z88FileStream.indexOf(escF, fileNamePos);
            if (fileDataPos == -1) {
                // ESC F marker not found in received data stream (beginning of file data)...
                return false;
            } else {
                // fetch remote Z88 filename ESC N <filename> ESC F
                z88Filename.clear();
                z88Filename.append(z88FileStream.mid(fileNamePos+2, fileDataPos-fileNamePos-1));
            }
        }

        // build true file image by parsing received byte stream
        fileWritten = false;
        for (int b = fileDataPos+2; b<z88FileStream.length(); b++) {
            int byte = z88FileStream[b];

            switch(byte) {
                case 27:
                    byte = z88FileStream[++b];

                    switch(byte) {
                        case 'B':
                            // parse ESC B XX sequence
                            byte = xtod(z88FileStream[b+1])*16 + xtod(z88FileStream[b+2]);
                            b += 2;
                            remoteFile.append(byte);
                            break;

                        case 'E':
                        case 'Z':
                            // ESC E (or ESC Z) is end of file

                            QString hostFilename = QString(hostPath).append((z88Filename.constData()+6));
                            QFile hostFile(hostFilename);

                            if (hostFile.exists() == true) {
                                // automatically replace existing host file
                                hostFile.remove();
                            }

                            if (hostFile.open(QIODevice::WriteOnly) == true) {
                                hostFile.write(remoteFile);     // write entire collected remote file contents to host file
                                hostFile.close();
                                emit impExpRecFile_Done(hostFilename);

                                fileWritten = true;
                                fileNamePos = b; // next filename is from this position in the filename
                            } else {
                                qDebug() << "receiveFile(): Couldn't open File '" << hostFilename << "' for writing - aborting...";
                                return false;
                            }
                            break;
                    }
                    break;

                default:
                    remoteFile.append(byte);
            }

            if (fileWritten == true) break;
        }
    }

    return true;
}


/*************************************************************************************
 *      Internal utility function: Generate data stream for Z88 Imp/Export Protocol.
 *      Caller must ensure that the filename applies to Z88 standard.
 *      The data provided by this function is used by sendImpExpFileData()
 *************************************************************************************/
QByteArray Z88SerialPort::createImpExpFileData(const QString &z88Filename, QByteArray rawFileData)
{
    QByteArray sendFileSequence;

    sendFileSequence.append(escN);
    sendFileSequence.append(z88Filename.toUtf8());
    sendFileSequence.append(escF);

    for (int b=0; b<rawFileData.length(); b++) {
        int byte = rawFileData[b] & 0xff;
        switch(byte) {
            case 1:
            case 2:
            case 5:
            case 6:
                // these are pre-amble PCLINK and EazyLink command bytes.. encode them as well
            sendFileSequence.append(escB).append( (QString( "%1" ).arg( byte, 2, 16, QChar('0') ).toUpper()).toUtf8() );
                break;
            case 10:
            case 13:
                // Line feeds are sent raw (so the the line counter increases in the popdown)
                sendFileSequence.append(byte);
                break;
            case 0x1b:
                // send ESC B 1B sequence
                sendFileSequence.append(escB).append("1B");
                break;
            case 0x11:
                // Ensure to use ESC B 11 for Xon byte
                sendFileSequence.append(0x1b).append("B11");
                break;
            case 0x13:
                // Ensure to use ESC B 13 for Xoff byte
                sendFileSequence.append(0x1b).append("B13");
                break;
            default:
                if (byte > 126) {
                    // bytes in range 127 - 255 must be ESC B encoded (7bit data flow to Imp/Export)
                    sendFileSequence.append(escB).append( (QString( "%1" ).arg( byte, 2, 16, QChar('0') ).toUpper()).toUtf8() );
                } else {
                    sendFileSequence.append(byte);
                }
        }
    }
    sendFileSequence.append(escE); // End Of File - this function only sends a single file..

    return sendFileSequence;
}


/*************************************************************************************
 *      Send a prepared file stream to Z88 using Imp/Export Protocol.
 *      The data stream was prepared using createImpExpFileData()
 *      Before executing this function, Imp/Export popdown is running B)atch receive
 *************************************************************************************/
bool Z88SerialPort::sendImpExpFileData(QByteArray sendFileSequence)
{
    const int maxz88cps = 512; // For Imp/Export, lowest common denominator CPS speed on OZ V3, 9600BPS
    bool fileTransfered = true;
    QElapsedTimer timer;

    if (transmitting == true) {
        qDebug() << "sendImpExpFileData(): Transmission already ongoing with Z88 - aborting...";
        fileTransfered = false;
    } else {
        //qDebug() << "sendImpExpFileData(): Transmitting data to Z88 file...";

        transmitting = true;
        // record the total file stream size, which can be fetched by getTotalFileTransferStream()
        int totalFileTransferStream = sendFileSequence.length();

        for (int offset=0; offset<totalFileTransferStream; offset += maxz88cps) {
            timer.start(); // record time for file stream transfer

            if (sendBytes(sendFileSequence.mid(offset,maxz88cps)) == true) {
                // record the time taken for transfer of block stream capacity for 1s (maxz88cps)
                fileBufferFlushTime = timer.elapsed();
                emit totalBytesWritten(offset+maxz88cps);

                wait( 1000 - fileBufferFlushTime ); // wait for average time to send a block
            } else {
                qDebug() << "sendFileData(): file contents not transmitted properly to Z88!";
                fileTransfered = false;
                break;
            }
        }

        // file transmitted to Imp/Export popdown (successfully or not)...
        transmitting = false;
    }

    return fileTransfered;
}


/*****************************************************************************
 *      EazyLink Server V4.4
 *      Send a file to Z88 using EazyLink protocol
 *      Caller must ensure that the filename applies to Z88 standard
 *****************************************************************************/
bool Z88SerialPort::uploadHostFile(const QString &z88Filename, QString hostFilename, bool lfConvProtLvl9)
{
    bool fileTransfered = false;
    QFile hostFile(hostFilename);

    if (hostFile.exists() == true) {
        if (transmitting == true) {
            qDebug() << "sendFile(): Transmission already ongoing with Z88 - aborting...";
        } else {
            if (hostFile.open(QIODevice::ReadOnly) == true) {
                // file opened for read-only...
                QByteArray fileContents = hostFile.readAll();
                hostFile.close();

                fileTransfered = sendFileData(z88Filename, fileContents, lfConvProtLvl9);
            } else {
                qDebug() << "sendFile(): Couldn't open File for reading - aborting...";
            }
        }

    } else {
        qDebug() << "sendFile(): File doesn't exist - aborting...";
    }

    return fileTransfered;
}



/*****************************************************************************
 *      EazyLink Server V4.4
 *      Send a file to Z88 using EazyLink protocol
 *      Caller must ensure that the filename applies to Z88 standard
 *****************************************************************************/
bool Z88SerialPort::sendFileData(const QString &z88Filename, QByteArray &fileContents, bool lfConvProtLvl9)
{
    const int maxz88cps_9600_ozv3 = 512; // highest average characters per second speed on OZ V3, 9600BPS
    const int maxz88cps_9600_ozv441 = 916; // highest average characters per second speed on OZ V4.4.1+, 9600BPS
    const int maxz88cps_38400 = 2743; // highest average characters per second speed on OZ V4.4.1+, 38.400BPS
    int maxz88cps;

    if (eazylinkProtocolLevel >= 9) {
        // use CRC block transfer at 38400 BPS when available..
        return sendFileDataCrc(z88Filename, fileContents, lfConvProtLvl9);
    }

    bool fileTransfered = false;
    QByteArray sendFileSequence(sendFilesCmd);
    QElapsedTimer timer;

    sendFileSequence.append(escN);
    sendFileSequence.append(z88Filename.toUtf8());
    sendFileSequence.append(escF);

    if (transmitting == true) {
        qDebug() << "sendFileData(): Transmission already ongoing with Z88 - aborting...";
    } else {
        //qDebug() << "sendFileData(): Transmitting data to '" << z88Filename << "' Z88 file...";

        if ( sendCommand(sendFileSequence,2000) == false) {
            qDebug() << "sendFile(): EazyLink send file command not acknowledged - aborting...";
            return false;
        }

        sendFileSequence.clear();
        sendFileSequence.reserve(fileContents.length() + 4096); // extra space for ESC B xx sequences..
        transmitting = true;

        for (int b=0; b<fileContents.length(); b++) {
            switch(fileContents.at(b)) {
                case 1:
                case 2:
                case 5:
                case 6:
                    if (eazylinkProtocolLevel >= 8) {
                        // these are pre-amble PCLINK and EazyLink command bytes.. encode them as well
                            sendFileSequence.append(escB).append( (QString( "%1" ).arg( fileContents.at(b), 2, 16, QChar('0') ).toUpper()).toUtf8() );
                    } else {
                        // Earlier versions of EazyLink just needs to receive these raw...
                        sendFileSequence.append(fileContents.at(b) & 0xff);
                    }
                    break;

                case 0x1b:
                    // send ESC ESC sequence
                    sendFileSequence.append(escEsc);
                    break;
                case 0x11:
                    if (eazylinkProtocolLevel >= 8) {
                        // Only EazyLink 5.3, protocol level 08 understands the ESC B xx sequence
                        sendFileSequence.append(0x1b);
                        sendFileSequence.append("B");
                        sendFileSequence.append("11");
                    } else {
                        // Earlier versions of EazyLink just receive XON byte (it's H/W handshake, so it doesnt matter)...
                        sendFileSequence.append(fileContents.at(b) & 0xff);
                    }
                    break;
                case 0x13:
                    if (eazylinkProtocolLevel >= 8) {
                        // Only EazyLink 5.3, protocol level 08 understands the ESC B xx sequence
                        sendFileSequence.append(0x1b);
                        sendFileSequence.append("B");
                        sendFileSequence.append("13");
                    } else {
                        // Earlier versions of EazyLink just receive XOFF byte (it's H/W handshake, so it doesnt matter)...
                        sendFileSequence.append(fileContents.at(b) & 0xff);
                    }
                    break;
                default:
                    sendFileSequence.append(fileContents.at(b) & 0xff);
            }
        }
        sendFileSequence.append(escE); // Mark End Of File - this function only sends a single file..

        // record the total file stream size, which can be fetched by getTotalFileTransferStream()
        totalFileTransferStream = sendFileSequence.length();

        // define transfer capacity timings for Characters Per Second, based on popdown protocol and the buffer flush elapsed time
        if (eazylinkProtocolLevel >= 8) {
            // EazyLink popdown as part of OZ V4.4.1+
            if (getSerialPortSpeed() == 9600) {
                maxz88cps = maxz88cps_9600_ozv441;
            } else {
                // 38.400 BPS
                maxz88cps = maxz88cps_38400;
            }
        } else {
            // Eazylink popdown is running on OZ V4 or lower, per definition
            maxz88cps = maxz88cps_9600_ozv3;
        }

        for (int offset=0; offset<totalFileTransferStream; offset += maxz88cps) {
            timer.start(); // record time for file stream transfer

            if (sendBytes(sendFileSequence.mid(offset,maxz88cps)) == true) {
                // record the time taken for transfer of block stream capacity for 1s (maxz88cps)
                fileBufferFlushTime = timer.elapsed();
                emit totalBytesWritten(offset+maxz88cps);
                wait( 1000 - fileBufferFlushTime ); // wait for average time to send a block
            } else {
                qDebug() << "sendFileData(): file contents not transmitted properly to Z88!";
                transmitting = false;
                return false;
            }
        }

        if (sendBytes(escZ) == true) {
            // End of Batch, signal end of this file transmission
            fileTransfered = true;
        }
    }

    transmitting = false;
    return fileTransfered;
}


/**
 * @brief Internal utility to convert host text file to z88 text file
 * @param fileContents
 * @return new file contents
 */
QByteArray Z88SerialPort::convert2z88TextFile(const QByteArray &fileContents)
{
    QByteArray z88TextFile;
    z88TextFile.reserve(fileContents.length());

    for (int b=0; b<fileContents.length(); b++) {
        if (fileContents.at(b) == '\r') {
            z88TextFile.append(fileContents.at(b));

            if ( ((b+1)<fileContents.length()) && (fileContents.at(b+1) == '\n')) {
                // skip LF of CRLF
                b++;
            }
        } else if (fileContents.at(b) == '\n') {
            // replace LF with CR
            z88TextFile.append('\r');
        } else {
            z88TextFile.append(fileContents.at(b) & 0xff);
        }
    }

    return z88TextFile;
}


/*****************************************************************************
 * EazyLink Server V5.4 protocol level 9 (OZ V4.5+)
 * Send a file to Z88 using EazyLink protocol level 9, sending 128byte blocks
 * with acknowledge from popdown, using CRC-16 checksum validation of block by
 * receiver:
 *
 * From Client:
 * => ESC b ESC N <filename> ESC F
 *
 * While File data {
 *    => ESC [ <data> ESC N <HEX-CRC16> ESC ]
 *    <data> contains ESC ESC to send an ESC
 *    ESC Y <= block received OK, flushed to Z88 file
 *    ESC Z <= block received incorrectly, re-send block
 * }
 *
 * => ESC E   (file EOF)
 *
 * Caller must ensure that the filename applies to Z88 standard
 *****************************************************************************/
bool Z88SerialPort::sendFileDataCrc(const QString &z88Filename, QByteArray &fileContents, bool lfConvProtLvl9)
{
    bool fileTransfered = false;
    QByteArray dataBlockSequence(sendFilesCmd);
    QByteArray dataBlock;
    int b;
    int blockNo = 1, totalBytesSent = 0;
    Crc16 crc;

    dataBlockSequence.append(escN);
    dataBlockSequence.append(z88Filename.toUtf8());
    dataBlockSequence.append(escF);

    if (transmitting == true) {
        qDebug() << "sendFileDataCrc(): Transmission already ongoing with Z88 - aborting...";
    } else {
        //qDebug() << "sendFileDataCrc(): Transmitting data to '" << z88Filename << "' Z88 file...";

        if ( sendCommand(dataBlockSequence,2000) == false) {
            qDebug() << "sendFileDataCrc(): EazyLink send file command not acknowledged - aborting...";
            return false;
        }

        // signal that file block transfer is progressing..
        transmitting = true;

        if (lfConvProtLvl9 == true) {
            // protocol level 9 only, when CRLF conversion is enabled:
            //      filter LF in CRLF sequence
            //      replace LF with CR

            qDebug() << "sendFileDataCrc(): Convert text file to z88 format, before CRC-block transfer";
            fileContents = convert2z88TextFile(fileContents);
        }

        b=0; // byte index of file content
        int fileLength = fileContents.length();
        while ( b<fileLength ) {
            // build a sequence of approx. 118 bytes
            dataBlock.clear();
            dataBlock.reserve(192);
            int bi=b;
            int csLen = 0; // total length of (sub) block of fileContents, used for checksum calculation
            for (; bi<(b+118) && bi<fileLength; bi++) {
                switch(fileContents.at(bi)) {
                    case 1:
                    case 5:
                            // pre-amble PCLINK II & EazyLink command bytes.. encode  it
                        dataBlock.append(escB).append( (QString( "%1" ).arg( fileContents.at(bi), 2, 16, QChar('0') ).toUpper()).toUtf8());
                        break;

                    case 0x1b:
                        // send ESC B 1B sequence
                        dataBlock.append(esc1b);
                        break;

                    default:
                        dataBlock.append(fileContents.at(bi) & 0xff);
                }

                csLen++; // total bytes of original (non-sequenced) file content built in this data block

                if (dataBlock.length() >= 118) {
                    // the current data block has reached 128 bytes or more... time to send it..
                    break;
                }
            }

            // get CRC-16 of sub-block of fileContens (the non-ESC encoded file contents)
            crc.resetChecksum();
            crc.checksum(fileContents, b, csLen);
            QString hexCrc16 = QString("%1").arg(crc.getChecksum(), 4, 16, QChar('0')).toUpper();

            // build complete CRC-16 block (data is approx 118 bytes + 10 bytes post-amble
            dataBlockSequence.clear();
            dataBlockSequence.reserve(256);
            dataBlockSequence.append(escCrcStart);
            dataBlockSequence.append(dataBlock);
            dataBlockSequence.append(escN);
            dataBlockSequence.append( hexCrc16.toUtf8() ); // 4 byte Hex Ascii string of CRC-16
            dataBlockSequence.append(escCrcEnd);

            int blockRetryNo = 0;
            while (blockRetryNo<8) {
                qDebug() << "sendFileDataCrc():"<< z88Filename <<"): Sending CRC block #" << blockNo << "(" << hexCrc16 <<"), size " << dataBlockSequence.size();
                if (sendBytes(dataBlockSequence) == true) {
                    // CRC data block sent, wait for acknowledge...
                    totalBytesSent += dataBlockSequence.size();
                    if (waitforResponse(1500, escY) == true) { // give the Z88 max 2s to respond, enough time for Z88 to flush bytes to file
                        // acknowledge was received, investigate..
                        if (response.at(1) == 'Y') {
                            // EazyLink reported successful block reception...
                            qDebug() << "sendFileDataCrc():"<< z88Filename <<"): CRC block #" << blockNo << ", acknowledged.";
                            break;
                        }
                        if (response.at(1) == 'Z') {
                            // try to send block again
                            qDebug() << "sendFileDataCrc():"<< z88Filename <<"): CRC block #" << blockNo << " incorrect CRC, retry attempt #" << ++blockRetryNo << ", block re-transmit!";
                        }
                        if (response.at(1) == 'F') {
                            // EazyLink reporter file I/O error, abort file transmission
                            qDebug() << "sendFileDataCrc():"<< z88Filename <<"): CRC block #" << blockNo << ": EazyLink report file I/O error while saving buffer data, abort file!";
                            transmitting = false;
                            return false;
                        }
                    } else {
                        qDebug() << "sendFileDataCrc():"<< z88Filename <<"): CRC block #" << blockNo << ", acknowledge response not received (timeout), retry attempt #" << ++blockRetryNo << ", block re-transmit!";
                    }
                } else {
                    qDebug() << "sendFileDataCrc():"<< z88Filename <<"): CRC block #" << blockNo << ", file contents not transmitted properly to Z88!";
                    transmitting = false;
                    return false;
                }
            }

            if (blockRetryNo == 8) {
                qDebug() << "sendFileDataCrc():"<< z88Filename <<"): CRC block #" << blockNo << ", retry-attempts exceed or no response from Z88!";
                transmitting = false;
                return false;
            }

	    if ((blockNo % 11) == 0) {
               // update progress bar every 11th block transmitted
               emit totalBytesWritten(b);
            }

            // build a new block to transmit...
            b = bi+1;
            blockNo++;
        }

        // All blocks transmitted, send End Of File & End Of Batch - this function only sends a single file..
        wait(100); // wait 0.1s, before signalling end of file transfer

        dataBlockSequence.clear();
        dataBlockSequence.append(escE);
        dataBlockSequence.append(escZ);
        if (sendBytes(dataBlockSequence) == true) {
            fileTransfered = true;
            totalFileTransferStream = totalBytesSent;
            qDebug() << "sendFileDataCrc():"<< z88Filename <<"): Total bytes sent: " << totalBytesSent;
        } else {
            qDebug() << "sendFileDataCrc(): file contents not transmitted properly to Z88!";
        }

        // file transmitted to EazyLink popdown (successfully or not)...
        transmitting = false;
    }

    return fileTransfered;
}


/*****************************************************************************
 *      EazyLink Server V5.2, protocol level 06
 *      Get CRC-32 of Z88 file
 *****************************************************************************/
QByteArray Z88SerialPort::getFileCrc32(const QString &filename, int z88FileSize)
{
    QList<QByteArray> fileSizeList;
    QByteArray fileCrc32String;

    // EazyLink's CRC-32 functionality processes 10100 bytes/s (worst case on EPR), rounded up...
    // if file size is unknown, use default 1min
    float timeoutMs = (z88FileSize > 0) ? (z88FileSize / 10100 * 1000) + 1500: 60000;

    QByteArray crc32FileCmdRequest = crc32FileCmd;
    crc32FileCmdRequest.append(filename.toUtf8());
    crc32FileCmdRequest.append(escZ);

    if ( sendCommand(crc32FileCmdRequest,1000) == true) {
        if (transmitting == true) {
            qDebug() << "getFileCrc32(): Transmission already ongoing with Z88 - aborting...";
        } else {
            // receive crc-32 into list
            receiveListItems(fileSizeList, timeoutMs);

            if (fileSizeList.count() > 0) {
                fileCrc32String = fileSizeList.at(0); // the "list" has only one item...
            }

            transmitting = false;
        }
    }

    return fileCrc32String;
}


/*****************************************************************************
 *      EazyLink Server V5.2, protocol level 06
 *      Get Device Information; free memory / total size, returned in list
 *****************************************************************************/
QList<QByteArray> Z88SerialPort::getDeviceInfo(const QString &device)
{
    QList<QByteArray> infoList;

    QByteArray devInfoCmdPath = devInfoCmd;
    devInfoCmdPath.append(device.toUtf8());
    devInfoCmdPath.append(escZ);

    if ( sendCommand(devInfoCmdPath,2000) == true) {
        if (transmitting == true) {
            qDebug() << "getDeviceinfo(): Transmission already ongoing with Z88 - aborting...";
        } else {
            // receive device information elements into list
            // first element is free memory in byte, second element is device size in Kb
            // if device was not found, an empty list is returned.
            receiveListItems(infoList,5000);
            transmitting = false;
        }
    }

    return infoList;
}


/*********************************************************************************************************
 *      Private class methods
 ********************************************************************************************************/

/**
 * @brief Listen max X seconds for incoming bytes on serial port
 * @param timeOutSeconds
 * @return true if bytes are available in serial port
 */
bool Z88SerialPort::isBytesAvailable(const int timeOutSeconds)
{
    QTime timeout = QTime::currentTime().addSecs(timeOutSeconds);

    // wait max <timeOutSeconds> for incoming bytes
    while(QTime::currentTime() < timeout) {
        //QCoreApplication::processEvents(QEventLoop::AllEvents, 10);
        if (port->waitForReadyRead(90)) {
            return true;
        }
        QCoreApplication::processEvents(QEventLoop::AllEvents, 10);
    }

    return false;
}


/*****************************************************************************
 *      Synchronize with Z88 before sending command
 *****************************************************************************/
bool Z88SerialPort::synchronize()
{
    bool synchronized = false;
    int retrycount = 3;

    if (portOpenStatus == true) {
        if (transmitting == true) {
            qDebug() << "synchronize(): Transmission already ongoing with Z88 - aborting synchronization...";
        } else {
            transmitting = true;
            z88AvailableStatus = false;

            // try 3 times to wait for synch response in 3s
            while ((synchronized == false) && (retrycount-- > 0)) {
                if (sendBytes(synchEazyLinkProtocol, 1000) == false) {
                    qDebug() << "synchronize(): Unable to synchronize with Z88 (" << retrycount << "more retries..)";
                } else {
                    if (waitforResponse(1000, synchEazyLinkProtocol) == true) {
                        synchronized = true; // synch response correct received from Z88
                        z88AvailableStatus = true;
                    } else {
                        qDebug() << "synchronize(): Bad synchronize response from Z88: [" << response << "]," << response.count() << " bytes (" << retrycount << "more retries..)";
                    }
                }
            }

            transmitting = false;
        }
    }

    return synchronized;
}



/**
 * @brief fetch response from Z88, wait max timeoutMs milliseconds
 * @param timeoutMs
 * @return true if specified end of response was received (or ESC Z), else false (possible timeout)
 */
bool Z88SerialPort::waitforResponse(const int timeoutMs, QByteArray endOfResponse)
{
    bool responseReceived = false;
    QByteArray escEscResponse = escEsc + endOfResponse.at(1);
    QByteArray escEscZ = escEsc + 'Z';

    // reset elapsed timer before start to received bytes from the serial port
    timeSinceLastReceivedByte.start();

    // wait at least <timeoutMs> for correct response (but return immediately if response is available)
    while( (timeoutMs - timeSinceLastReceivedByte.elapsed()) > 0 ) {
        if ( ((response.endsWith(endOfResponse) == true) || (response.endsWith(escZ) == true)) &&
             !((response.endsWith(escEscResponse) == true) || (response.endsWith(escEscZ) == true))) {
            // look for true ending of ESC 'Z' or ESC X, not part of sequence ESC ESC 'Z' (which means an ESC were sent, followed by ´Z')
            // EazyLink protocol level 8 or lower may send ESC ESC for ESC. Protocol level 9 always send ESC B 1B for ESC.
            responseReceived = true;
            break;
        }
        port->waitForReadyRead(90);
        QCoreApplication::processEvents(QEventLoop::AllEvents, 10);
    }

    // return what was received from the serial port
    return responseReceived;
}


/**
 * @brief wait for X ms (do nothing)
 * @param timeoutMs
 */
void Z88SerialPort::wait(const int timeoutMs)
{
    QTime timeout = QTime::currentTime().addMSecs(timeoutMs);
    while(QTime::currentTime() < timeout) {
        port->waitForReadyRead(90);
        QCoreApplication::processEvents(QEventLoop::AllEvents, 10);
    }
}


/*****************************************************************************
 *      Send a command string to the Z88
 *****************************************************************************/
bool Z88SerialPort::sendCommand(QByteArray cmd, int timeoutMs)
{
    bool commandSent = false;

    if ( synchronize() == true ) {
        if (transmitting == true) {
            qDebug() << "sendCommand(): Transmission already ongoing with Z88 - aborting...";
        } else {
            transmitting = true;

            if ( sendBytes(cmd, timeoutMs) == false) {
                qDebug() << "sendCommand(): Command not transmitted properly to Z88!";
                z88AvailableStatus = false;
            } else {
                commandSent = true;
                z88AvailableStatus = true;
            }

            transmitting = false;
        }
    } else {
        z88AvailableStatus = false;
    }

    return commandSent;
}


/**
 * @brief Send byte(s) to Z88
 * @param byteSequence
 * @return returns true if bytes were successfully transmitted
 */
bool Z88SerialPort::sendBytes(QByteArray byteSequence, int timeoutMs)
{
    // reset elapsed timer before start to define a timeout, if no bytes are sent on serial port
    timeSinceLastReceivedByte.start();

    // initialize transmit buffer variables that are used by SLOT handlers
    response.clear();
    bytesSent = false;
    totalBytesSent = 0;
    totalBytesToSend = byteSequence.length();
    int bytesWritten = port->write(byteSequence);

    while ( (bytesSent == false) && ((timeoutMs - timeSinceLastReceivedByte.elapsed()) > 0) ) {
        // wait for data to be written...
        port->waitForBytesWritten(90);
        QCoreApplication::processEvents(QEventLoop::AllEvents, 10);
    }

    if (bytesWritten == -1) {
        qDebug() << "sendBytes(): no bytes transmitted to Z88!";
        return false;
    }
    if (bytesWritten != byteSequence.length()) {
        qDebug() << "sendBytes(): not all bytes transmitted to Z88!";
        return false;
    }

    return true;
}


/*****************************************************************************
 *      Helper function to receive list items in the following format:
 *          ESC N ... [ESC N ...] ESC Z  {1 or more elements}
 *
 *          Get Z88 Devices
 *          Get Z88 Directories
 *          ...
 *****************************************************************************/
bool Z88SerialPort::receiveListItems(QList<QByteArray> &list, int timeoutMs)
{
    QByteArray item;

    list.clear();

    waitforResponse(timeoutMs, escZ);

    if ( response.length() > 0 ) {
        int b=0;
        while( b < response.length() ) {
            switch((char) response[b]) {
                case 27:
                    b++;
                    switch((char) response[b]) {
                        case 'N':
                            if (item.length() > 0) {
                                list.append(item);          // Current item finished
                                item.clear();               // get ready for a new item (name)
                            }
                            break;

                        case 'Z':
                            if (item.length() > 0) {
                                list.append(item);
                            }
                            return true;                     // end of items - exit

                        default:
                            qDebug() << "receiveListItems(): Unknown ESC terminator encountered!";
                            return false;                    // illegal escape command - abort
                    }
                    break;

                default:
                    item.append((char) response[b]);   // new byte collected in current item
            }

            b++; // point to next byte in response
        }

        return true;
    } else {
        return false;
    }
}


char Z88SerialPort::xtod(char c)
{
    if (c>='0' && c<='9') return c-'0';
    if (c>='A' && c<='F') return c-'A'+10;
    if (c>='a' && c<='f') return c-'a'+10;
    return c=0;        // not Hex digit
}
