/*********************************************************************************************

 EazyLink2 - Fast Client/Server Z88 File Management
 Copyright (C) 2012-2015 Gunther Strube (gstrube@gmail.com) & Oscar Ernohazy

 EazyLink2 is free software; you can redistribute it and/or modify it under the terms of the
 GNU General Public License as published by the Free Software Foundation;
 either version 2, or (at your option) any later version.
 EazyLink2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.
 You should have received a copy of the GNU General Public License along with EazyLink2;
 see the file COPYING. If not, write to the
 Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

**********************************************************************************************/

#include <QDebug>
#include "setupwizard.h"

/**
  * The Base Setup Wizard constructor.
  * @param prefs is the preferences dialog that will get configured.
  * @param parent is the owner widget.
  */
SetupWizard::SetupWizard(Prefrences_dlg *prefs, QWidget *parent) :
    QWizard(parent),
    m_pref_dlg(prefs),
    m_serSelectPage(NULL)
{
    /**
      * Create a Mac style Wizard.
      */
    setWizardStyle(QWizard::MacStyle);

    setOption(QWizard::NoBackButtonOnStartPage );

    /**
      * Add the Intro Page
      */
    addPage(new SetupWiz_Intro(this));

    /**
      * Add the Mode Selection Page
      */
    addPage(new SetupWiz_ModeSelect(this));

    /**
      * Add the Serial Select Page
      */
    addPage(m_serSelectPage = new SetupWiz_SerialSelect(this, this));

    /**
      * Add the Final Summary Page
      */
    addPage(new SetupWiz_FinalPage(this, this));   
}

/**
  * Event that get's called when user hits accept to end.
  */
void SetupWizard::accept()
{
    QDialog::accept();
}

/**
  * Save the Selected Serial port name
  * @param shortname is the portname without the path.
  */
void SetupWizard::setPort(const QString &shortname)
{
    m_portname = shortname;
}

/**
  * Get the portname that was saved by setPort().
  * @return the short port name.
  */
const QString &SetupWizard::getPort() const
{
    return m_portname;
}

