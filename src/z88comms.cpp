/*********************************************************************************************

 EazyLink2 - Fast Client/Server Z88 File Management
 Copyright (C) 2012-2015 Gunther Strube (gstrube@gmail.com) & Oscar Ernohazy

 EazyLink2 is free software; you can redistribute it and/or modify it under the terms of the
 GNU General Public License as published by the Free Software Foundation;
 either version 2, or (at your option) any later version.
 EazyLink2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.
 You should have received a copy of the GNU General Public License along with EazyLink2;
 see the file COPYING. If not, write to the
 Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

**********************************************************************************************/

#include <qdebug.h>
#include <QListIterator>
#include <QDate>

#include "mainwindow.h"
#include "crc32.h"
#include "z88filedate.h"
#include "z88filespec.h"
#include "desktop_view.h"
#include "z88_devview.h"
#include "prefrences_dlg.h"
#include "actionsettings.h"
#include "z88serialport.h"
#include "z88comms.h"


/**
  * The Z88 Communications Constructor.
  * @param port is a reference to the Z88 Serial port class to use for I/O.
  * @param parent is the owner QT Object.
  */
Z88Comms::Z88Comms(Z88SerialPort &port, MainWindow *parent)
  :QObject(parent),
   m_sport(port),
   m_curOP(OP_idle),
   m_prevOP(OP_idle),
   m_redo_lastCmd(false),
   m_enaFilesize(false),
   m_enaTimeDate(false),
   m_enaPromtUser(0),
   m_byteTranslation(false),
   m_linefeedConversion(false),
   m_xferFileprogress(0),
   m_loaded_default_dev(false),
   m_z88Selections(NULL),
   m_z88RenDelSelections(NULL),
   m_deskSelections(NULL),
   m_z88Sel_itr(NULL),
   m_deskSel_itr(NULL),
   m_z88rendel_itr(NULL),
   m_mainWindow(parent),
   m_dest_isDir(false),
   m_runCnt(0)
{

    connect(&m_sport,
            SIGNAL(impExpRecFilename(const QString &)),
            this,
            SLOT(impExpRecFilename(const QString &)) );

    connect(&m_sport,
            SIGNAL(impExpRecFile_Done(const QString &)),
            this,
            SLOT(impExpRecFile_Done(const QString &)) );
}

/**
  * The Destructor
  */
Z88Comms::~Z88Comms()
{
}

/**
  * The Communications thread Method.
  */
void Z88Comms::run(comOpcodes_t execOp)
{
    QString msg;

    m_runCnt++;

    emit enableCmds(false, m_sport.isOpen());

    m_curOP = execOp;

    switch(execOp){
        case OP_idle:           // Comms Thread is Idle.
            break;
        case OP_reopen:         // Re-open the Serial port.            
            run(OP_openDevName);
            if(m_redo_lastCmd && m_sport.isOpen()){
                m_redo_lastCmd = false;
                run(m_prevOP);
            }
            break;
        case OP_openDevName:    // Open the Specified Serial Device.
            msg = "Trying to Open Port ";
            msg += m_devname;
            cmdStatus(msg);
            emit open_result(m_devname, m_sport.openSerialPort(m_devname, m_bps, true));
            break;
        case OP_openTestEzProto:
        {
            QStringListIterator i(m_PortScanList);
            int portIdx=-1;
            bool rc = false;

            cmdStatus("Looking for Z88...");

            while(i.hasNext()){
                portIdx++;
                QString pname(i.next());

                emit openTest_Start(portIdx);

                if(m_sport.openSerialPort(pname,9600, false)){
                    rc = m_sport.helloZ88();
                    m_sport.closeSerialport();
                }

                if(rc){
                    break;
                }
            }
            emit openTest_result(portIdx, rc);
        }
        break;
        case OP_openTestAscii:
        {
            QStringListIterator i(m_PortScanList);
            int portIdx=-1;
            bool rc = false;

            cmdStatus("Looking for Z88...");

            while(i.hasNext()){
                portIdx++;
                QString pname(i.next());

                /* Notify Prefs panel */
                emit openTest_Start(portIdx);

                /**
                  * Open the Port without HW flow control, so we don't hang
                  * When there is nothing attached.
                  */
                if(m_sport.openSerialPort(pname,9600, false)){
                    m_sport.closeSerialport();
                }
            }
            emit openTest_result(PT_RC_RAW_SCAN_COMPLETE, rc);
        }
        break;

        case OP_helloZ88:       // Send a Z88 Hello Command.
        {
            cmdStatus("Sending Hello Z88");
            bool rc = m_sport.helloZ88();

            if (rc == true) {
                if ((m_sport.getEazylinkPopdownProtocolLevel() >= 9) && (m_mainWindow->get_Prefs().get_AutoSpeed38400() == true)) {
                    // EazyLink popdown report that 38.400 is supported, switch to it if end-user allows it from settings...
                    // protocol level 8 also has 38400 support, but were less stable on those OZ versions)
                    run(OP_use38400bps);
                }
            } else {
                // EazyLink popdown didn't respond... there are more error scenarios...
                switch(m_sport.getSerialPortSpeed()) {
                    case 9600:
                        if ((m_sport.getEazylinkPopdownProtocolLevel() >= 9) && (m_mainWindow->get_Prefs().get_AutoSpeed38400() == true)) {
                            // the popdown is probably at 38400 from a previous run of the client... try to switch to it
                            qDebug() << "hello didn't respond at 9600, try 38400 BPS";
                            m_sport.closeSerialport();
                            m_bps = 38400;
                            m_sport.openSerialPort(m_devname, m_bps, true);
                            rc = m_sport.helloZ88();
                        }
                        break;

                    case 38400:
                        // If we're here, its only because the popdown is protocol level 9...
                        if ((m_sport.getEazylinkPopdownProtocolLevel() >= 9) && (m_mainWindow->get_Prefs().get_AutoSpeed38400() == true)) {
                            qDebug() << "hello didn't respond at 38400 BPS, try 9600 BPS";
                            m_sport.closeSerialport();
                            m_bps = 9600;
                            m_sport.openSerialPort(m_devname, m_bps, true);
                            rc = m_sport.helloZ88();
                        }
                        break;
                }
            }
            emit boolCmd_result("HelloZ88", rc);
        }
        break;

        case OP_use38400bps:   // Instruct EazyLink popdown to change serial port speed to 38.4000 BPS
        {
            if (m_bps < 38400) {
                // current connection is 9600, so go lightspeed...

                cmdStatus("Requesting to change to 38.400 BPS");
                bool rc = m_sport.use38400bps();
                if (rc == true) {
                    m_sport.closeSerialport();
                    m_bps = 38400;
                    m_sport.openSerialPort(m_devname, 38400, true);
                    m_sport.helloZ88(); // re-fetch protocol details (and set availability status)
                }
                emit boolCmd_result("Change to 38.400 BPS", rc);
            }
        }
        break;

        case OP_quitZ88:        // Request the Z88 Shutdown the Eazylink Popdown.
        {
            if (m_sport.isZ88Available() == true) {
                cmdStatus("Sending Z88 Quit EazyLink");
                m_sport.quitZ88();
                m_sport.closeSerialport();
            }
        }
        break;

        case OP_reloadTransTable:   // Reload the Z88 Translation Table
            cmdStatus("Sending Reload Translation Table");
            emit boolCmd_result("Reload Translation Table", m_sport.reloadTranslationTable());
            break;
        case OP_setZ88Clock:        // Set the Z88 Clock to the Desktop Time.
            cmdStatus("Syncing Z88 Clock to host Time");
            emit boolCmd_result("Z88 Clock Sync", m_sport.setZ88Time());
            break;
        case OP_getZ88Clock:        // Read the Z88 Real-time Clock.
        {
            QList<QByteArray> tm_date;

            cmdStatus("Reading Z88 Clock");
            tm_date = m_sport.getZ88Time();
            if(tm_date.count()==2){
                msg = "Z88 Date and Time: ";
                msg += tm_date[0];
                msg += " - ";
                msg += tm_date[1];
                cmdStatus(msg);
            }
            else{
                emit boolCmd_result("Reading Z88 Clock", false);
            }
        }
            break;

        case OP_getDevices:         // Read the Available Z88 Storage Devices.
        {
            QList<QByteArray> *devlist = new QList<QByteArray>;
            cmdStatus("Searching for Z88 Storage Devices.");

            /**
              * Read Devices
              */
            devlist->append(m_sport.getDevices());
            emit Z88Devices_result(devlist, "");

            emit boolCmd_result("Reading Z88 Devices", !devlist->isEmpty());

            break;
        }
        case OP_getDirectories:     // Read the Directories available on the Z88 Device.
        {
            QList<QByteArray> *dirlist = new QList<QByteArray>;
            cmdStatus("Searching for Z88 Directories.");

            /**
              * Read Directories
              */
            bool status;
            dirlist->append(m_sport.getDirectories(m_z88devname, status));
            emit Z88Dir_result(m_z88devspec, dirlist);

            emit boolCmd_result("Reading Z88 Directories", !dirlist->isEmpty());
            break;
        }
        case OP_getDevInfo:     // Get the Free Space info for a device.
        {
            QList<QByteArray> *devInfolist = new QList<QByteArray>;

            devInfolist->append(m_sport.getDeviceInfo(m_z88devname.mid(0,6)));

            if(devInfolist->count() == nfo_max){
                emit Z88DevInfo_result(m_z88devname,
                                       devInfolist->at(dev_free).toLong(),
                                       devInfolist->at(dev_total).toLong() * BYTES_PER_K);
            }
            break;
        }
        case OP_getFilenames:       // Read the Filenames on the Z88 Storage Device.
        {
            bool retc = false;
            QList<QByteArray> *filelist = new QList<QByteArray>;
            cmdStatus("Reading Z88 Files...");

            /**
              * Read All Files Recursively
              */
            filelist->append(m_sport.getFilenames(m_z88devname, retc));

            QList<Z88FileSpec> *fileSpeclist = new QList<Z88FileSpec>;

            QListIterator<QByteArray> i(*filelist);

            int count = filelist->count();
            int idx = 0;
            int extmode = m_enaFilesize ? 1 : 0;
            extmode += m_enaTimeDate ? 1 : 0;

            QString msg = "Reading Z88 file ";
            if(m_enaFilesize){
                msg += "Size";
            }
            if(extmode > 1){
                msg += " & ";
            }
            if(m_enaTimeDate){
                msg += "Date";
            }
            msg += " info from ";
            msg += m_z88devname.mid(0,6);

            while(i.hasNext()) {
                idx ++;
                QString fname(i.next());
                QString fsize;
                QString fcdate;
                QString fmdate;

                /**
                  * If filesize is requested
                  */
                if(m_enaFilesize){
                    QString msg = QString("[%1] Reading File Size: (%2 of %3)").arg(fname.mid(0,6)).arg(idx).arg(count);
                    cmdStatus(msg);
                    /* Format the Size to a nicely formated integer */
                    fsize = QLocale(QLocale::system()).toString(m_sport.getFileSize(fname).toLongLong());
                }

                /**
                  * If time and date requested
                  * Read it from all devices except Eproms (time & date is only available in Z88 RAM filing system)
                  */
                if (m_enaTimeDate) {
                    if (!fname.contains("EPR")) {
                        QString msg = QString("[%1] Reading File Date: (%2 of %3)").arg(fname.mid(0,6)).arg(idx).arg(count);

                        cmdStatus(msg);

                        QList<QByteArray> fileTD(m_sport.getFileDateStamps(fname));
                        if(fileTD.count() == 2){
                            QDate qdt;
                            QTime qtm;

                            /**
                              * Convert the Eazylink date format to local format.
                              */
                            qdt = QDate::fromString(fileTD[0].mid(0,10), "dd/MM/yyyy");
                            qtm = QTime::fromString(fileTD[0].mid(11), "hh:mm:ss");

                            if(qdt.isValid()){
                                fcdate = QLocale(QLocale::system()).toString(qdt, QLocale::ShortFormat);

                                if(qtm.isValid()){
                                    fcdate += " " + QLocale(QLocale::system()).toString(qtm, QLocale::ShortFormat);
                                }
                            }
                            else{
                                fcdate = fileTD[0];
                            }

                            qdt = QDate::fromString(fileTD[1].mid(0,10), "dd/MM/yyyy");
                            qtm = QTime::fromString(fileTD[1].mid(11), "hh:mm:ss");

                            if(qdt.isValid()){
                                fmdate = QLocale(QLocale::system()).toString(qdt, QLocale::ShortFormat);

                                if(qtm.isValid()){
                                    fmdate += " " + QLocale(QLocale::system()).toString(qtm, QLocale::ShortFormat);
                                }
                            }
                            else{
                                fmdate = fileTD[1];
                            }
                        }                        
                    } else {
                        // Files on a Flash/Eprom Card only supports file sizes.
                        fcdate = fmdate = "not supported";
                    }
                }

                fileSpeclist->append(Z88FileSpec(fname, fsize, fcdate, fmdate));
            }

            emit Z88FileSpeclist_result(m_z88devspec, fileSpeclist);

            emit boolCmd_result("Reading Z88 Files", retc);
            break;
        }
        case OP_getZ88FileTree:     // Get the entire Z88 File tree.
        {
            QList<QByteArray> *infolist = new QList<QByteArray>;
            cmdStatus("Reading Z88 Info.");

            infolist->append(m_sport.getEazyLinkZ88Version());
            if(infolist->count()!=1){
                emit boolCmd_result("Reading Z88 Version", false);
            }
            else{
                m_EzSvr_Version = infolist->first();
            }

            QList<QByteArray> *devlist = new QList<QByteArray>;
            cmdStatus("Reading Z88 File System");

            /**
              * Read Devices
              */
            devlist->append(m_sport.getDevices());

            /**
              * Load the Default Device as the first display only the first time
              * Eazylink2 is run
              */
            QString def_dev;
            if(!m_loaded_default_dev && !devlist->isEmpty()){
                /**
                  * Get the Default device
                  */
                QList<QByteArray> devname = m_sport.getRamDefaults();
                if(!devname.isEmpty()){
                    def_dev = devname.first();
                }
                m_loaded_default_dev = true;
            }

            emit Z88Devices_result(devlist, def_dev);

            if(devlist->isEmpty()){
                emit boolCmd_result("Reading Z88 Devices", false);
                break;
            }

            /**
              * Read the Directories for each device
              */
            QListIterator<QByteArray> i(*devlist);      

            while(i.hasNext()){
                /**
                  * If Available, get the Free device space
                  */
                _getDevInfo(i.peekNext());

                run(_getDirectories(i.next()));
            }

            /**
              * Read the Files From the Device
              */
            i.toFront();

            while(i.hasNext()){
                run(_getFileNames(i.next()));
            }
            emit boolCmd_result("Z88 Refresh", true);

            break;
        }
        case OP_initreceiveFiles:       // Start the Receive Files Process
        {
            delete m_z88Sel_itr;
            m_z88Sel_itr = new QMutableListIterator<Z88_Selection> (*m_z88Selections);
            m_z88Sel_itr->toFront();
            m_xferFileprogress = 0;

            /** ensure that current translation mode is set on Z88 before actual transfer begins.. */
            if(!BYTE_TranslationEnable(m_byteTranslation = m_mainWindow->get_Prefs().get_Byte_Trans())){
                break;
            }

            /** ensure that current CRLF mode is also set ... */
            if(!CRLF_TranslationEnable(m_linefeedConversion = m_mainWindow->get_Prefs().get_CRLF_Trans())){
                break;
            }

            // drop through
        }

        case OP_receiveFiles:       // Receive files from the Z88
        {
            if(m_z88Sel_itr->hasNext()){

                const Z88_Selection &z88sel(m_z88Sel_itr->peekNext());
                QString srcname(z88sel.getFspec());
                QString dest = m_destPath + "/" + z88sel.getRelFspec();

                QString cmdLine;
                const ActionRule *arule = m_mainWindow->get_Prefs().findActionRule(Action_Settings::ActKey_RX_FROMZ88, dest, cmdLine);

                if(arule){
                    /**
                      * Skip the file Requested.
                      */
                    if(arule->m_RuleID == ActionRule::IGNORE){
                        run(OP_receiveNext);
                        break;
                    }

                    /**
                      * Allow for User Expanded cmd line.
                      * Don't use cmd line with the Open On....
                      */
                    if(arule->m_RuleID != ActionRule::OPEN_WITH_EXT_APP){
                        QString newcmd;
                        merge_relFspec(cmdLine, z88sel.getRelFspec(), newcmd);
                        dest = m_destPath + "/" + newcmd;
                    }
                }

                if(shouldPromptUser(z88sel, dest)){
                    emit PromptReceiveSpec(srcname, dest, &m_enaPromtUser);
                    break;
                }

                run(OP_receiveFile);
            }
            break;
        }
        case OP_receiveFile:        // Get the Specified file.
        {
            Z88SerialPort::retcode rc;
            QString cmdLine;
            const ActionRule *arule;

            do{
                const Z88_Selection &z88sel(m_z88Sel_itr->next());
                QString srcname(z88sel.getFspec());

                // before receiving the file, we need the file size in order to initialize the progress bar
                int fileSize = m_sport.getFileSize(srcname).toInt();
                QString fileSizeStr = fileSize < 1024 ? QString("%1 bytes").arg(fileSize): QString("%1Kb").arg(fileSize/1024);
                QString msg = "Receiving " + srcname + " to " + m_destPath + "  (" + fileSizeStr + ")";

                QList<QByteArray> z88fileTD;

                /**
                  * See if the File Already Exists and Skip the File if Needed.
                  */
                if((m_enaPromtUser & FILE_EXISTS) && (m_enaPromtUser & (NO_TO_OW_ALL))){
                    goto skip2;
                }

                cmdLine.clear();
                arule = m_mainWindow->get_Prefs().findActionRule(Action_Settings::ActKey_RX_FROMZ88, m_destPath + "/" + z88sel.getRelFspec(), cmdLine);

                if(arule){
                    if(arule->m_RuleID == ActionRule::TRANSFER_FILE){ // Receive Default
                        bool ena = m_mainWindow->get_Prefs().get_CRLF_Trans();

                        if(m_linefeedConversion != ena){
                            if(!CRLF_TranslationEnable(ena)){
                                break;
                            }
                            m_linefeedConversion = ena;
                        }

                        ena = m_mainWindow->get_Prefs().get_Byte_Trans();

                        if(m_byteTranslation != ena){
                            if(!BYTE_TranslationEnable(ena)){
                                break;
                            }
                            m_byteTranslation = ena;
                        }
                    }
                    else{
                        if(arule->m_RuleID == ActionRule::CONVERT_CRLF){ // Convert CRLF
                            if(!m_linefeedConversion){
                                if(!CRLF_TranslationEnable(true)){
                                    break;
                                }
                            }
                            m_linefeedConversion = true;
                        }
                        else{
                            if(arule->m_RuleID == ActionRule::BINARY_MODE){  // RX Binary
                                if(m_linefeedConversion){
                                    if(!CRLF_TranslationEnable(false)){
                                        break;
                                    }
                                }
                                m_linefeedConversion = false;

                                if(m_byteTranslation){
                                    if(!BYTE_TranslationEnable(false)){
                                        break;
                                    }
                                    m_byteTranslation = false;
                                }
                            }
                        }
                    }
                }

                /**
                  * Receive the file from Z88
                  */

                m_mainWindow->initFileProgressBar(fileSize);
                QObject::connect(&m_sport,  SIGNAL(totalBytesRead(int)), m_mainWindow, SLOT(fileBytesReceived(int)));
                cmdStatus(msg);

                if(arule && arule->m_RuleID != ActionRule::OPEN_WITH_EXT_APP){
                    QString newcmd;
                    merge_relFspec(cmdLine, z88sel.getRelFspec(), newcmd);
                    rc = m_sport.receiveFile(srcname, m_destPath, newcmd, m_dest_isDir);
                }
                else{
                    rc = m_sport.receiveFile(srcname, m_destPath, z88sel.getRelFspec(), m_dest_isDir);
                }

                QObject::disconnect(&m_sport,  SIGNAL(totalBytesRead(int)), m_mainWindow, SLOT(fileBytesReceived(int)));
                m_mainWindow->disableFileProgressBar();

                m_xferFileprogress++;

                /**
                  * Post RX Processing Action Handling
                  */
                m_mainWindow->get_Prefs().execActions(Action_Settings::ActKey_RX_FROMZ88,  m_destPath + "/" + z88sel.getRelFspec(), cmdLine );
                emit boolCmd_result("Transfer", (rc == Z88SerialPort::rc_done));

                if(rc != Z88SerialPort::rc_done){
                    //qDebug() << "Transfer rc=" << rc;
                    break;
                } else {
                    if ( isBinaryFile(m_destPath + "/" + z88sel.getRelFspec()) ) {
                        // host file is binary, perform a CRC-32 check and compare with remote CRC-32 on Z88 file
                        // But only if Translation is OFF
                        if ( m_mainWindow->get_Prefs().get_Crc32File() && (m_byteTranslation == false) ) {
                            if (m_sport.getEazylinkPopdownProtocolLevel() >= 6) {
                                uint hfileCrc32 = hostFileCrc32(m_destPath + "/" + z88sel.getRelFspec());
                                uint hfileSize = hostFileFileSize(m_destPath + "/" + z88sel.getRelFspec());

                                m_mainWindow->enableBusyIndicator();
                                uint z88fileCrc32 = z88FileCrc32(srcname, fileSize, hfileSize);
                                m_mainWindow->disableBusyIndicator();

                                // only compare CRC-32 of host & z88 file, when a CRC-32 was returned of Z88 file
                                if ( (z88fileCrc32 != 0xffffffff) && (hfileCrc32 != z88fileCrc32)) {
                                    // binary file download failed...
                                    emit boolCmd_result( QString("Crc-32 of '%1' failed").arg(m_destPath + "/" + z88sel.getRelFspec()), false);
                                    break;
                                }
                            }
                        }
                    }
                }
skip2:
                // remove successfully processed item from list
                m_z88Sel_itr->remove();

                if(m_z88Sel_itr->hasNext()){
                    const Z88_Selection &z88selnxt(m_z88Sel_itr->peekNext());
                    QString relFspec(z88selnxt.getRelFspec());
                    QString dest = m_destPath + "/" + relFspec;

                    QString cmdLine;
                    arule = m_mainWindow->get_Prefs().findActionRule(Action_Settings::ActKey_RX_FROMZ88, dest, cmdLine);

                    if(arule){
                        /**
                          * Skip the file Requested.
                          */
                        if(arule->m_RuleID == ActionRule::IGNORE){
                            run(OP_receiveNext);
                            break;
                        }

                        /**
                          * Allow for User Expanded cmd line.
                          * Don't use cmd line with the Open On....
                          */
                        if(arule->m_RuleID != ActionRule::OPEN_WITH_EXT_APP){
                            QString newcmd;
                            merge_relFspec(cmdLine, relFspec, newcmd);
                            dest = m_destPath + "/" + newcmd;
                        }
                    }

                    if(shouldPromptUser(z88selnxt, dest)){
                        srcname = z88selnxt.getFspec();
                        emit PromptReceiveSpec(srcname, dest, &m_enaPromtUser);
                        break;
                    }
                }
                else{
                    emit boolCmd_result("File Transfer", true);
                }

            } while(m_z88Sel_itr->hasNext());

            break;
        }
        case OP_receiveNext:        // Receive the Next File from the Z88.
        {
            /**
              * Skip the current file
              */
            if(m_z88Sel_itr->hasNext()){
                m_z88Sel_itr->next();
                m_xferFileprogress++;
            }

            /**
              * Receive the next file
              */
            if(m_z88Sel_itr->hasNext()){
                run(OP_receiveFiles);
            }

            break;
        }
        case OP_dirLoadDone:        // The Desktop Dir read is complete event.
            setState_Idle();
            emit DirLoadComplete(false);
            if(--m_runCnt <=0){
                m_runCnt = 0;
              //Oscar  emit enableCmds(true, m_sport.isOpen());
            }
            return;  // Don't re-enable commands here

        case OP_initsendFiles:      // Start the Send Files to Z88 Process.
        {
            delete m_deskSel_itr;
            m_deskSel_itr = new QMutableListIterator<DeskTop_Selection> (*m_deskSelections);
            m_deskSel_itr->toFront();
            m_xferFileprogress = 0;

            /** ensure that Z88 time is equal to desktop time, before transfering file to Z88 */
            if (m_mainWindow->get_Prefs().get_AutoSyncClock() && m_sport.syncZ88Time() == true){
                cmdStatus("Z88 Time has been synchronised with desktop time");
            }

            /** ensure that current translation mode is set on Z88 before actual transfer begins.. */
            if(!BYTE_TranslationEnable( m_byteTranslation = m_mainWindow->get_Prefs().get_Byte_Trans())){
                break;
            }

            /** ensure that current CRLF mode is also set ... */
            if(!CRLF_TranslationEnable( m_linefeedConversion = m_mainWindow->get_Prefs().get_CRLF_Trans())){
                break;
            }

            // drop through
        }
        case OP_sendFiles:          // Send Files to the Z88.
        {
            if(m_deskSel_itr->hasNext()){

                const DeskTop_Selection &desksel(m_deskSel_itr->peekNext());
                QString destFspec = m_destPath + desksel.getFname();
                QString srcname(desksel.getFspec());

                QString cmdLine;
                const ActionRule *arule = m_mainWindow->get_Prefs().findActionRule(Action_Settings::ActKey_TX_TOZ88, destFspec, cmdLine);

                if(arule){
                    /**
                      * Skip the file Requested.
                      */
                    if(arule->m_RuleID == ActionRule::IGNORE){
                        run(OP_sendNext);
                        break;
                    }

                    if(arule->m_RuleID != ActionRule::OPEN_WITH_EXT_APP){
                        destFspec = cmdLine;
                    }
                }

                /**
                  * Prompt the User for transfer and / or for Overwrite
                  */
                if(shouldPromptUser(desksel, destFspec))
                {
                    emit PromptSendSpec(srcname, destFspec, &m_enaPromtUser);
                    break;
                }

                run(OP_sendFile);
            }
            break;
        }
        case OP_sendFile:           // Send a File To the Z88.
        {
            do{
                const DeskTop_Selection &desksel(m_deskSel_itr->next());
                QString srcname(desksel.getFspec());
                QFile hostFile(srcname);

                QString msg = "Sending ";
                QString fileSize = hostFile.size() < 1024 ? QString("%1 bytes").arg(hostFile.size()): QString("%1Kb").arg(hostFile.size()/1024);
                msg += srcname + " to " + m_destPath + "  (" + fileSize + ")";

                /**
                  * Send the file to Z88
                  */
                bool rc;
                QString destFspec = m_destPath + desksel.getFname();
                QString transferredMsg = "File transfer";

                if(desksel.getType() == DeskTop_Selection::type_Dir){
                    rc = m_sport.createDir(destFspec);
                    if(!rc){
                        rc = !m_sport.isFileAvailable(destFspec);
                    }
                }
                else {
                    /**
                      * See if the File Already Exists and Skip the File if Needed.
                      */
                    if((m_enaPromtUser & FILE_EXISTS) && (m_enaPromtUser & (NO_TO_OW_ALL))){
                        goto skip1;
                    }

                    QString cmdLine;
                    uint hfCrc32 = -1;

                    bool binaryFileStatus = isBinaryFile(srcname);
                    if ( (binaryFileStatus == true) && (m_mainWindow->get_Prefs().get_Crc32File()) ) {
                        m_linefeedConversion = m_byteTranslation = false;

                        if (m_sport.getEazylinkPopdownProtocolLevel() >= 6)
                            // Before uploading, get CRC-32 of hostfile (to be compared with uploaded binary file on Z88)
                            hfCrc32 = hostFileCrc32(srcname);

                        // qDebug() << "OP_sendFile: this is a binary file";
                        // linefeed conversion OFF, translation OFF for this file
                        if(!CRLF_TranslationEnable(false)){
                            // abort, command not ack'ed.
                            break;
                        }
                        if(!BYTE_TranslationEnable(false)){
                            break;
                        }
                    } else {
                        // it's a text file, let rule system decide conversion semantics..
                        const ActionRule *arule = m_mainWindow->get_Prefs().findActionRule(Action_Settings::ActKey_TX_TOZ88, destFspec, cmdLine);

                        if(arule) {

                            if(arule->m_RuleID == ActionRule::TRANSFER_FILE){ // send Default
                                bool ena = m_mainWindow->get_Prefs().get_CRLF_Trans();

                                if(m_linefeedConversion != ena){
                                    if(!CRLF_TranslationEnable(ena)){
                                        break;
                                    }
                                    m_linefeedConversion = ena;
                                }

                                ena = m_mainWindow->get_Prefs().get_Byte_Trans();

                                if(m_byteTranslation != ena){
                                    if(!BYTE_TranslationEnable(ena)){
                                        break;
                                    }
                                    m_byteTranslation = ena;
                                }
                            }
                            else{
                                if(arule->m_RuleID == ActionRule::CONVERT_CRLF){ // Convert CRLF
                                    if(!m_linefeedConversion){
                                        if(!CRLF_TranslationEnable(true)){
                                            break;
                                        }
                                    }
                                    m_linefeedConversion = true;
                                }
                                else{
                                    if(arule->m_RuleID == ActionRule::BINARY_MODE){ // TX Binary
                                        if(m_linefeedConversion){
                                            if(!CRLF_TranslationEnable(false)){
                                                break;
                                            }
                                        }
                                        m_linefeedConversion = false;

                                        if(m_byteTranslation){
                                            if(!BYTE_TranslationEnable(false)){
                                                break;
                                            }
                                            m_byteTranslation = false;
                                        }
                                    }
                                }
                            }

                            if(arule->m_RuleID == ActionRule::OPEN_WITH_EXT_APP){
                                qDebug() <<  "Open With [" << cmdLine <<  "] Not Implemented Yet.";
                            }
                            else{
                                if(!cmdLine.isEmpty()){
                                    destFspec =  cmdLine;
                                }
                            }
                        }
                    }

                    cmdStatus(msg);

                    if(m_sport.isFileAvailable(destFspec)){
                        // delete file before uploading
                        m_mainWindow->enableBusyIndicator();
                        rc = m_sport.deleteFileDir(destFspec);
                        m_mainWindow->disableBusyIndicator();
                    }

                    m_mainWindow->initFileProgressBar(hostFile.size());
                    QObject::connect(&m_sport,  SIGNAL(totalBytesWritten(int)), m_mainWindow, SLOT(fileBytesSent(int)));
                    rc = m_sport.uploadHostFile(destFspec, srcname, m_linefeedConversion);
                    QObject::disconnect(&m_sport,  SIGNAL(totalBytesWritten(int)), m_mainWindow, SLOT(fileBytesSent(int)));
                    m_mainWindow->disableFileProgressBar();

                    if( (rc == true) && (binaryFileStatus == true) && (m_mainWindow->get_Prefs().get_Crc32File()) && (m_sport.getEazylinkPopdownProtocolLevel() >= 6)) {
                        // binary file was successfully uploaded to Z88, validate contents with CRC-32
                        m_mainWindow->enableBusyIndicator();
                        uint z88Crc32 = z88FileCrc32(destFspec, 0, hostFile.size());
                        m_mainWindow->disableBusyIndicator();

                        // only compare CRC-32 of host & z88 file, when a CRC-32 was returned of Z88 file
                        if ( (z88Crc32 != 0xffffffff) && (hfCrc32 != z88Crc32)) {
                            // binary file upload failed...
                            rc = false;
                            transferredMsg += " (CRC-32 of uploaded file incorrect) ";
                        }
                    }

                    if (rc == true) {
                        // remote update Z88 file create & update date using desktop file datestamps
                        QFileInfo hostFileInfo(hostFile);

                        // execute this functionality silently (if it fails, dont interrupt upload flow)
                        m_sport.setFileDateStamps(
                                    destFspec,
                                    Z88FileDate::fromHostDate(hostFileInfo.birthTime()).toLatin1(),
                                    Z88FileDate::fromHostDate(hostFileInfo.lastModified()).toLatin1()
                                );
                    }
                }

                emit boolCmd_result(transferredMsg, rc);

                if(!rc){
                    break;
                }
skip1:             
                m_xferFileprogress++;
                // remove selected (processed) desktop item from list
                m_deskSel_itr->remove();

                if(m_deskSel_itr->hasNext()){
                    const DeskTop_Selection &deskselnxt(m_deskSel_itr->peekNext());

                    QString destFspec = m_destPath + deskselnxt.getFname();

                    QString cmdLine;
                    const ActionRule *arule = m_mainWindow->get_Prefs().findActionRule(Action_Settings::ActKey_TX_TOZ88, destFspec, cmdLine);

                    if(arule){
                        /**
                          * Skip the file Requested.
                          */
                        if(arule->m_RuleID == ActionRule::IGNORE){
                            run(OP_sendNext);
                            break;
                        }

                        if(arule->m_RuleID == ActionRule::OPEN_WITH_EXT_APP){
                            destFspec = cmdLine;
                        }
                    }

                    if(shouldPromptUser(deskselnxt, destFspec)){
                        srcname = deskselnxt.getFspec();
                        emit PromptSendSpec(srcname, destFspec, &m_enaPromtUser);
                        break;
                    }
                }
                else{

                    /**
                     * Refresh the Device view
                     */
                    emit refreshSelectedZ88DeviceView();

                    emit boolCmd_result("File Transfer", true);
                }

            } while(m_deskSel_itr->hasNext());

            break;
        }
        case OP_sendNext:           // Send the Next File to the Z88.
        {
            /**
              * Skip the current file
              */
            if(m_deskSel_itr->hasNext()){
                m_deskSel_itr->next();
                m_xferFileprogress++;
            }

            /**
              * Send the next file
              */
            if(m_deskSel_itr->hasNext()){
                run(OP_sendFiles);
            }
            else{

                /**
                 * Refresh the Device view
                 */
                 emit refreshSelectedZ88DeviceView();
            }

            break;
        }
        case OP_createDir:          // Create A Directory on the Z88
        {
            emit cmdStatus("Creating Directory " + m_z88devspec);

            bool rc;

            m_mainWindow->enableBusyIndicator();
            rc = m_sport.createDir(m_z88devspec);
            m_mainWindow->disableBusyIndicator();

            /**
              * Try to create a Directory, otherwise find out why it couldn't be created.
              */
            if(rc){
                /**
                 * Refresh the Device view
                 */
                emit refreshSelectedZ88DeviceView();
            }
            else{
                bool status;
                QList<QByteArray> dirs(m_sport.getDirectories(m_z88devspec, status));
                QListIterator<QByteArray> i(dirs);

                while(i.hasNext()){
                    if(QString(i.next()) == m_z88devspec){
                        QString msg("Directory " + m_z88devspec + " Already exists!");

                        emit cmdStatus(msg);
                        emit displayCritError("Cannot Create Directory.\n" + msg);
                        goto done;
                    }
                }
            }

            emit boolCmd_result("Make Directory", rc);
done:
            break;
        }

        case OP_crc32Files:     // Start the Crc-32 of files on Z88 Process.
        {
            QMutableListIterator<Z88_Selection> *z88crc32_itr = new QMutableListIterator<Z88_Selection> (*m_z88Selections);
            z88crc32_itr->toFront();

            if(z88crc32_itr->hasNext()){

                Z88_Selection z88sel(z88crc32_itr->peekNext());
                QString srcfspec(z88sel.getFspec());

                if(srcfspec.size() > 6){
                    if(z88sel.getType() == Z88_DevView::type_File) {
                        m_mainWindow->enableBusyIndicator();
                        uint crc32 = z88FileCrc32(srcfspec);
                        m_mainWindow->disableBusyIndicator();

                        if (crc32 != 0xffffffff) {
                            emit boolCmd_result("CRC-32 of " + srcfspec + " = " + QString("%1,").arg(crc32, 0, 16).toUpper(), true);
                        }
                    }
                }
            }

            break;
        }

        case OP_initrenameDirFiles:     // Start the Rename dir or files on Z88 Process.
        {
            delete m_z88rendel_itr;
            m_z88rendel_itr = new QMutableListIterator<Z88_Selection> (*m_z88RenDelSelections);

            m_z88rendel_itr->toFront();
            // Drop Through
        }
        case OP_renameDirFiles:         // Rename Files or Directories on the Z88.
        {
            if(m_z88rendel_itr->hasNext()){
                emit PromptRename(m_z88rendel_itr);
            }

            break;
        }
        case OP_renameDirFile:          // Rename a File or Dir on the Z88.
        {
            bool rc = true;

            if(!m_destPath.isEmpty()){
                m_mainWindow->enableBusyIndicator();
                rc = m_sport.renameFileDir(m_z88devspec, m_destPath);
                m_mainWindow->disableBusyIndicator();

                if(rc){                 
                    emit renameZ88Item(&(m_z88rendel_itr->peekNext()), m_destPath);
                }
                else{
                    int idx = m_z88devspec.lastIndexOf('/');
                    if(idx > -1){
                        m_destPath = m_z88devspec.mid(0, idx) + '/' + m_destPath;
                    }

                    if(m_z88rendel_itr->peekNext().getType() == Z88_DevView::type_Dir){
                        bool status;
                        QList<QByteArray> dirs(m_sport.getDirectories(m_destPath, status));
                        QListIterator<QByteArray> i(dirs);

                        while(i.hasNext()){
                            if(QString(i.next()) == m_destPath){
                                QString msg("Cannot Rename Directory.\n" + m_destPath + " Already exists!");
                                emit renameCmd_result(msg, rc);
                                break;
                            }
                        }
                    }
                    else{
                        /* Rename of File Failed */
                        if(m_sport.isFileAvailable(m_destPath)){
                            QString msg("Cannot Rename File.\n" + m_destPath + " Already exists!");
                            emit renameCmd_result(msg, rc);
                        }
                    }
                }
            }

            if(rc){
                m_z88rendel_itr->next();
                /**
                  * Do the Next one
                  */
                run(OP_renameDirFiles);
            }

            break;
        }
        case OP_initdelDirFiles:            // Start the Delete Files or Dirs on the z88 Process.
        {
            delete m_z88rendel_itr;
            m_z88rendel_itr = new QMutableListIterator<Z88_Selection> (*m_z88RenDelSelections);
            m_xferFileprogress = 0;
            m_z88rendel_itr->toFront();

            // Drop Through
        }
        case OP_delDirFiles:                // Delete files or Directories on the Z88.
        {
            if(m_z88rendel_itr->hasNext()){

                const Z88_Selection &z88sel(m_z88rendel_itr->peekNext());
                QString srcname(z88sel.getFspec());

                if(m_enaPromtUser & PROMPT_USER){
                    emit PromptDeleteSpec(srcname, (z88sel.getType() == Z88_DevView::type_Dir), &m_enaPromtUser);
                    break;
                }

                run(OP_delDirFile);
            }
            else{
                /**
                  * If Available, get the Free device space
                  */
                _getDevInfo(m_z88devspec);
            }

            break;
        }
        case OP_delDirFile:                // Delete a Directory or File on the Z88.
        {
            if(!m_z88RenDelSelections){
                break;
            }

            do{
                QTreeWidgetItem *item(m_z88rendel_itr->peekNext().getQtreeItem());
                Z88_Selection z88sel(m_z88rendel_itr->next());
                QString srcname(z88sel.getFspec());

                QString msg = "Deleting ";
                msg += srcname;
                emit cmdStatus(msg);

                /**
                  * Request the delete of the file / dir
                  */
                m_mainWindow->enableBusyIndicator();
                bool rc = m_sport.deleteFileDir(srcname);
                m_mainWindow->disableBusyIndicator();

                m_xferFileprogress++;

                if(rc){
                    emit deleteZ88Item(item);
                    emit boolCmd_result("Delete " + srcname, true);
                }
                else{
                    bool retc;
                    /**
                      * See if the dir is empty
                      */
                    if(z88sel.getType() == Z88_DevView::type_Dir){
                        int cnt = m_sport.getFilenames(srcname + '*', retc).count();
                        if(!retc){
                            emit boolCmd_result("Delete " + srcname, false);
                            goto done2;
                        }
                        if(cnt > 0){
                            rc = true;
                        }
                    }
                    else{
                        int cnt = m_sport.getFilenames(srcname , retc).count();
                        if(!cnt || !retc){
                            emit boolCmd_result("Delete " + srcname, false);
                            goto done2;
                        }
                    }
                }

                if(!rc){
                    m_z88rendel_itr->previous();
                    emit PromptDeleteRetry(srcname, (z88sel.getType() == Z88_DevView::type_Dir));
                    goto done2;
                    break;
                }

                if((m_enaPromtUser & PROMPT_USER) && m_z88rendel_itr->hasNext()){
                    const Z88_Selection &z88selnxt(m_z88rendel_itr->peekNext());

                    srcname = z88selnxt.getFspec();
                    emit PromptDeleteSpec(srcname, (z88sel.getType() == Z88_DevView::type_Dir), &m_enaPromtUser);
                    goto done2;
                }

            }while(m_z88rendel_itr->hasNext());

done2:

            /**
              * If Available, get the Free device space
              */
            _getDevInfo(m_z88devspec);

            break;
        }
        case OP_delDirFileNext:         // Delete Next Z88 Dir or file.
        {
            /**
              * Skip the current file
              */
            if(m_z88rendel_itr->hasNext()){
                m_z88rendel_itr->next();
                m_xferFileprogress++;
            }

            /**
              * Delete the next file
              */
            if(m_z88rendel_itr->hasNext()){
                run(OP_delDirFiles);
            }
            else{
                /**
                  * If Available, get the Free device space
                  */
                _getDevInfo(m_z88devspec);
            }

            break;
        }
        case OP_refreshZ88View:         // Refresh the Z88 View
        {
            /**
              * If Available, get the Free device space
              */
            _getDevInfo(m_z88devspec);

            run(_getDirectories(m_z88devspec));
            run(_getFileNames(m_z88devspec));
        }
        case OP_impExpSendFiles:
        {
            int cnt = m_ImpExp_srcList.count();
            bool rc = true;

            if(cnt){
                for(int idx = 0; idx < cnt; idx++ ){
                    QFile hostFile(m_ImpExp_srcList[idx]);

                    QString msg = "Sending ";
                    msg += m_ImpExp_dstList[idx];

                    emit cmdStatus("Imp-Export Sending: " + m_ImpExp_srcList[idx]);
                    // qDebug() << "dev=" << m_destPath << " src=" << m_ImpExp_srcList[idx] << " dst=" << m_ImpExp_dstList[idx];

                    QByteArray dataStream = m_sport.prepareImpExpFile(m_destPath + m_ImpExp_dstList[idx], m_ImpExp_srcList[idx]);;

                    m_mainWindow->initFileProgressBar(dataStream.size());
                    QObject::connect(&m_sport,  SIGNAL(totalBytesWritten(int)), m_mainWindow, SLOT(fileBytesSent(int)));

                    rc = m_sport.sendImpExpFileData(dataStream);

                    QObject::disconnect(&m_sport, SIGNAL(totalBytesWritten(int)), m_mainWindow, SLOT(fileBytesSent(int)));
                    m_mainWindow->disableFileProgressBar();

                    if(!rc){
                        emit boolCmd_result("Imp-Export Send " + m_ImpExp_srcList[idx], false);
                        break;
                    }
                }
                if(rc){
                    emit boolCmd_result("Imp-Export Send", rc);
                }
            }
            break;
        }
        case OP_impExpRecvFiles:
        {
            emit cmdStatus("Imp-Export Receiving into: " + m_destPath);

            bool rc = m_sport.impExpReceiveFiles(m_destPath);
            emit boolCmd_result("Imp-Export Receive", rc);
            break;
        }
    }

    if(m_curOP == OP_dirLoadDone){
        emit DirLoadComplete(true);
    }

    m_curOP = OP_idle;

    if(--m_runCnt <=0){
        m_runCnt = 0;
        emit enableCmds(true, m_sport.isOpen());
    }

    return;
}

/**
  * Set up the Progress bar Abort Handler
  */
void Z88Comms::SetupAbortHandler(QProgressDialog *pd)
{
    connect(pd, SIGNAL(canceled()), this, SLOT(CancelSignal()) );
}

/**
  * The Slot that gets called when user clicks abort on the progress Dialog.
  */
void Z88Comms::CancelSignal()
{
    AbortCmd();
}

/**
  * Imp-Export Protocol Receive File Start
  * @param fname is the File name that started.
  */
void Z88Comms::impExpRecFilename(const QString &fname)
{
    emit cmdStatus("Imp-Export receiving file: " + fname);
}

/**
  * Import Export Protocol Receive File Complete.
  * @param fname is the name of the Complete file.
  */
void Z88Comms::impExpRecFile_Done(const QString &fname)
{
    emit boolCmd_result("Imp-Export Receive " + fname, true);
}

/**
  * The Slot that gets called when user hits abort button on the main form.
  */
void Z88Comms::AbortCmd(const QString &msg)
{
    cmdStatus(msg);
}

/**
  * @return true if communication thread is busy, else false.
  */
bool Z88Comms::isBusy()
{
    /**
      * Make sure we are not running another command
      */
    if(m_sport.isTransmitting())
        return true;
    else
        return false;
}

/**
  * Method to test if the Communications Port is Open.
  * @return true if the port is open.
  */
bool Z88Comms::isOpen()
{
    return m_sport.isOpen();
}


/**
  * Re-init (open) the communications port, and optionally re-do the last command.
  * @param redo_lastcmd on call set to true to redo last command after open.
  * @return true if communication thread was idle.
  */
bool Z88Comms::reopen(bool redo_lastcmd)
{
    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return false;
    }

    if(m_devname.isEmpty()){
        return false;
    }

    m_redo_lastCmd = redo_lastcmd;
    run(OP_reopen);

    return true;
}

/**
  * Open the Com port with Hardware flow control.
  * @param devname is the full device path to open. ie /dev/tty.Keyspan1
  * @return true if communication thread was idle.
  */
bool Z88Comms::open(const QString &devname, int bps)
{
    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return false;
    }

    m_devname = devname;
    m_bps = bps;
    startCmd(OP_openDevName);

    return true;
}


/**
  * Start Scan for the Z88 Port.
  * @param portList is the List of ports to scan.
  * @param EzLink set to true to perform Eazylink Hello to test.
  * @return true if communication thread was idle.
  */
bool Z88Comms::scanForZ88(const QStringList &portList, bool EzLink)
{

    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return false;
    }

    m_PortScanList = portList;

    if(EzLink){
        startCmd(OP_openTestEzProto);
    }
    else{
        startCmd(OP_openTestAscii);
    }

    return true;
}

/**
  * Send Z88 Hello Command.
  * @return true if communication thread was idle.
  */
bool Z88Comms::helloZ88()
{

    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return false;
    }

    startCmd(OP_helloZ88);

    return true;
}


/**
  * Switch to 38.400 BPS
  * @return true if communication thread was idle.
  */
bool Z88Comms::use384000bps()
{
    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return false;
    }

    startCmd(OP_use38400bps);

    return true;
}


/**
  * Send Z88 ezlink Quit Command.
  * @return true if communication thread was idle.
  */
bool Z88Comms::quitZ88()
{

    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {

        return false;
    }

    startCmd(OP_quitZ88);

    return true;
}

/**
  * Reload Translation table command.
  * @return true if communication thread was idle.
  */
bool Z88Comms::ReloadTranslation()
{

    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return false;
    }

    startCmd(OP_reloadTransTable);

    return true;
}

/**
  * Set the Z88 Real time Clock to the Host computer time.
  * @return true if communication thread was idle.
  */
bool Z88Comms::setZ88Time()
{

    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return false;
    }

    startCmd(OP_setZ88Clock);

    return true;
}

/**
  * Get the Z88 Real Time Clock.
  * @return true if communication thread was idle.
  */
bool Z88Comms::getZ88Time()
{

    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return false;
    }

    startCmd(OP_getZ88Clock);

    return true;
}

/**
  * Read the Z88 Misc Information.
  * @return true if communication thread was idle.
  */
QList<QByteArray> *Z88Comms::getInfo()
{
    QByteArray info;
    QList<QByteArray> *infolist = new QList<QByteArray>;

    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return NULL;
    }

    cmdStatus("Reading Z88 Info.");

    info = m_sport.getEazyLinkZ88Version();
    if (info.length() > 0) {
        infolist->append(info);
    } else {
        emit boolCmd_result("Reading Z88 Version", false);
        return NULL;
    }

    /**
      * Read Z88 Free memory
      */
    info = m_sport.getZ88FreeMem();
    if (info.length() > 0) {
        infolist->append(info);
    } else {
        emit boolCmd_result("Reading Z88 Free Memory", false);
        return NULL;
    }

    QList<QByteArray> devlist = m_sport.getDevices();
    if (devlist.count() > 0) {
        infolist->append(devlist);
    } else {
        emit boolCmd_result("Reading Z88 Devices", false);
        return NULL;
    }

    emit boolCmd_result("Reading Z88 Info", true);
    return infolist;
}

/**
  * Get a list of the Storage Devices on the Z88.
  * @return true if communication thread was idle.
  */
bool Z88Comms::getDevices()
{

    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return false;
    }

    startCmd(OP_getDevices);

    return true;
}

/**
  * Refresh the Specified Z88 Device View.
  * @param devname is the name of the Z88 Sotrage device.
  */
bool Z88Comms::RefreshZ88DeviceView(const QString &devname)
{

    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        //qDebug() << "In RefreshZ88DeviceView but not Idle State=" << m_curOP;
        return false;
    }

    m_z88devspec = devname;

    startCmd(OP_refreshZ88View);

    return true;
}

/**
  * Create a Directory on the Z88.
  * @param dirname is the string name of the directory to create.
  * @return true if com thread isn't already busy.
  */
bool Z88Comms::mkDir(const QString &dirname)
{
    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return false;
    }

    m_z88devspec = dirname;

    startCmd(OP_createDir);

    return true;
}

/**
  * Rename a Z88 File or Directory.
  * @param oldname is the Current file or dir name.
  * @param newname is the New Name
  * @return true if the Comthread is not already busy.
  */
bool Z88Comms::renameFileDir(const QString &oldname, const QString &newname)
{

    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return false;
    }

    m_z88devspec = oldname;
    m_destPath = newname;

    startCmd(OP_renameDirFile);

    return true;
}

/**
  * Rename a list of Z88 Files or Directories.
  * z88Selections is a list of files or directories to rename.
  * @return true if the Comthread is not already busy.
  */
bool Z88Comms::renameFileDirectories(QList<Z88_Selection> *z88Selections)
{

    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return false;
    }

    m_z88RenDelSelections = z88Selections;

    startCmd(OP_initrenameDirFiles);

    return true;
}


/**
  * Get CRC-32 of a list of Z88 Files.
  * z88Selections is a list of files to retrieve CRC-32.
  * @return true if the Comthread is not already busy.
  */
bool Z88Comms::crc32Files(QList<Z88_Selection> *z88Selections)
{
    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return false;
    }

    m_z88Selections = z88Selections;

    startCmd(OP_crc32Files);

    return true;
}



/**
  * Retry Renaming a File or Directory, after an error.
  * @param next set this to true, to skip the current file or directory.
  * @return true if the com thread isn't already busy.
  */
bool Z88Comms::renameFileDirRety(bool next)
{

    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return false;
    }

    if(m_z88rendel_itr && m_z88rendel_itr->hasNext()){
        if(next){
            m_z88rendel_itr->next();
        }

        startCmd(OP_renameDirFiles);
    }
    return true;
}

/**
  * Delete a list of Files or Directories on the Z88.
  * @param z88Selections is the list of filenames or directories to delete.
  * @param prompt_usr set to true to prompt for each file or directory before deleting.
  * @return true if the Coms thread is not already busy.
  */
bool Z88Comms::deleteFileDirectories(QList<Z88_Selection> *z88Selections, uPrompt prompt_usr)
{

    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return false;
    }

    m_enaPromtUser = prompt_usr;
    m_z88RenDelSelections = z88Selections;

    /**
      * Save the Device Name from the First Entry
      */
    if(!m_z88RenDelSelections->isEmpty()){
        m_z88devspec = m_z88RenDelSelections->first().getFspec().mid(0,6);
    }

    startCmd(OP_initdelDirFiles);

    return true;
}

/**
  * Delete a File or Directory on the Z88.
  * @param next set to true to skip over the current file or dir.
  * @return true if the Coms thread is not already busy.
  */
bool Z88Comms::deleteFileDirectory(bool next)
{

    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return false;
    }

    if(next){
        startCmd(OP_delDirFileNext,false);
    }
    else{
        startCmd(OP_delDirFile,false);
    }

    return true;
}

bool Z88Comms::impExpSendFile(const QString &Z88_devname, const QStringList &z88Filenames, const QStringList &hostFilenames)
{
    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return false;
    }

    if(z88Filenames.count() != hostFilenames.count() || Z88_devname.isEmpty()){
        return false;
    }

    // discard any previous serial port connection & settings..
    m_sport.closeSerialport();
    // ensure that serial port is 9600, H/W handshaking
    m_sport.openSerialPort(m_devname, 9600, true);

    m_destPath = Z88_devname;
    m_ImpExp_dstList = z88Filenames;
    m_ImpExp_srcList = hostFilenames;

    startCmd(OP_impExpSendFiles);

    return true;
}

bool Z88Comms::impExpReceiveFiles(const QString &hostPath)
{

    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return false;
    }

    m_destPath = hostPath;

    startCmd(OP_impExpRecvFiles);

    return true;
}

/**
  * private get Z88 Directories Method.
  * @param devname is the Z88 Storage device to read.
  * @return the OP code to get directories.
  */
Z88Comms::comOpcodes_t Z88Comms::_getDirectories(const QString &devname){
    m_z88devname = devname;
    m_z88devname += "//*";
    m_z88devspec = devname;

    return (m_curOP = OP_getDirectories);
}

/**
  * private: get the Z88 Files on the Specified Device.
  * @param devname is the device to read.
  * @return the OP code to get directories.
  */
Z88Comms::comOpcodes_t Z88Comms::_getFileNames(const QString &devname)
{
    m_z88devname = devname;
    m_z88devname += "//*";
    m_z88devspec = devname;

    return(m_curOP = OP_getFilenames);
}

/**
  * Read the Specified Device Information (EazyLink protocol level 6+)
  * @param devname is the Device to Read info.
  * @return True if Serversupports this request, false otherwise
  */
bool Z88Comms::_getDevInfo(const QString &devname)
{
    /**
      * The First Version of The EazyLink Popdown that supports
      * Reading Device Info
      */
    if(m_sport.getEazylinkPopdownProtocolLevel() >= 6) {
        m_z88devname = devname;
        m_z88devname += "//*";
        m_z88devspec = devname;

        run(OP_getDevInfo);

        return true;
    }
    return false;
}

void Z88Comms::merge_relFspec(const QString &fname, const QString &relFspec, QString &result)
{
    int idx = relFspec.lastIndexOf("/");

    if(idx < 0){
        result = fname;
        return;
    }

    result = relFspec.mid(0,idx + 1);
    result += fname;
}

/**
  * Get the Directories from the Specified Z88 Device.
  * @return true if communication thread was idle.
  */
bool Z88Comms::getDirectories(const QString &devname)
{
    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return false;
    }

    startCmd(_getDirectories(devname));

    return true;
}

/**
  * Get Filenames from the Specified Z88 Device.
  * @return true if communication thread was idle.
  */
bool Z88Comms::getFileNames(const QString &devname)
{
    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return false;
    }

    startCmd(_getFileNames(devname));

    return true;
}

/**
  * Retreive the Entire File tree of directories and File Names on the Specified Device.
  * @param ena_size set to true to retreive file sizes also.
  * @param ena_date set to true to retreive file dates also.
  * @return true if communication thread was idle.
  */
bool Z88Comms::getZ88FileSystemTree(bool ena_size, bool ena_date)
{
    /**
      * Make sure we are not running another command
      */
    if( m_sport.isTransmitting()){
        return false;
    }

    comOpcodes_t op = OP_getZ88FileTree;

    if(m_enaFilesize != ena_size || m_enaTimeDate != ena_date){
        m_enaFilesize = ena_size;
        m_enaTimeDate = ena_date;
    }

    if(m_curOP == OP_idle){
        startCmd(op);
    }

    return true;
}

/**
  * Retrieve a List of Files from the Z88.
  * @param z88Selections is a list of files to retreive.
  * @param hostpath is the fully qualified directory on the host to save the files.
  * @parm prompt_usr set to true, to poll user for each file.
  * @return true if communication thread was idle.
  */
bool Z88Comms::receiveFiles(QList<Z88_Selection> *z88Selections, const QString &destpath, bool dest_isDir, uPrompt prompt_usr)
{

    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return false;
    }

    m_z88Selections = z88Selections;
    m_enaPromtUser = prompt_usr;
    m_destPath = destpath;
    m_dest_isDir = dest_isDir;

    startCmd(OP_initreceiveFiles);

    return true;
}

/**
  * Receive the Next file in the Selection list.
  * NOTE: only call this after a call to receiveFiles()
  * @param skip set to true to skip over the next file in the list.
  * @return true if the coms thread isn't already busy.
  */
bool Z88Comms::receiveFile(bool skip)
{
    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return false;
    }

    if(skip){
        startCmd(OP_receiveNext,false);
    }
    else{
        startCmd(OP_receiveFile,false);
    }
    return true;
}

/**
  * A callback event from the Dir read thread built into the Desk view.
  * @return true if the coms thread isn't already busy.
  */
bool Z88Comms::dirLoadComplete()
{

    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return false;
    }

    startCmd(OP_dirLoadDone, false);

    return true;
}

/**
  * Start the sending of a list of files from the Desktop to the Z88.
  * @param deskSelections is a list of files to transfer to the Z88.
  * @param destpath is the destination path on the Z88.
  * @param prompt_usr set to true on call to prompt before each file transfer.
  * @return true if the coms thread isn't already busy.
  */
bool Z88Comms::sendFiles(QList<DeskTop_Selection> *deskSelections, const QString &destpath, uPrompt prompt_usr)
{

    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return false;
    }

    delete m_deskSelections;

    m_deskSelections = deskSelections;
    m_enaPromtUser = prompt_usr;
    m_destPath = destpath;

    startCmd(OP_initsendFiles);

    return true;
}

/**
  * Send a File to the Z88.
  * @param skip set to true on call to skip the current entry.
  * @return true if the Comms thread isn't already busy.
  */
bool Z88Comms::sendFile(bool skip)
{
    /**
      * Make sure we are not running another command
      */
    if (m_sport.isTransmitting()) {
        return false;
    }

    if(skip){
        startCmd(OP_sendNext,false);
    }
    else{
        startCmd(OP_sendFile,false);
    }
    return true;
}

/**
  * Start an EazyLink Protocol Command
  * @param op is the Command op code
  * @param ena_resume set to true to allow a failure to resume from this command.
  */
void Z88Comms::startCmd(const Z88Comms::comOpcodes_t &op, bool ena_resume)
{
    if(ena_resume){
        m_prevOP = op;
    }

    run(op);
}

/**
  * Set the Current Operation State to Idle.
  * @return the Current Opcode.
  */
Z88Comms::comOpcodes_t Z88Comms::setState_Idle()
{
    comOpcodes_t curop = m_curOP;
    m_curOP = OP_idle;

    return curop;
}

/**
  * test to see if we should prompt the user for File Transfer
  * @param Source is the Desktop Source File.
  * @param destFspec is the Destination File on the Z88
  * @return true if A prompt is needed.
  */
bool Z88Comms::shouldPromptUser(const DeskTop_Selection &Source, const QString &destFspec)
{
    m_enaPromtUser &= ~FILE_EXISTS;

    if( ((m_enaPromtUser & NO_TO_OW_ALL) || !(m_enaPromtUser & YES_TO_OW_ALL)) &&
        (Source.getType() == DeskTop_Selection::type_File))
    {
        if(m_sport.isFileAvailable(destFspec)){
            m_enaPromtUser |= FILE_EXISTS;
        }
    }

    return  (Source.getType() == DeskTop_Selection::type_File) &&
            ((m_enaPromtUser & PROMPT_USER) ||
            ((m_enaPromtUser & FILE_EXISTS) &&
             !(m_enaPromtUser & (NO_TO_OW_ALL | YES_TO_OW_ALL))) );
}

/**
  * Test to seeif we should Prompt for User file transfer
  * @parm Source is the Z88 Selection
  * @parm destFspec is the destination on the desktop
  * @return true is prompt is needed
  */
bool Z88Comms::shouldPromptUser(const Z88_Selection &Source, const QString &destFspec)
{
    m_enaPromtUser &= ~FILE_EXISTS;

    if( ((m_enaPromtUser & NO_TO_OW_ALL) || !(m_enaPromtUser & YES_TO_OW_ALL)) &&
        (Source.getType() == Z88_DevView::type_File))
    {
        //qDebug() << " check to see if "<< destFspec ;
        /**
          * Check to see if the file exists on Desktop
          */
        QFile ofile(destFspec);

        if(ofile.exists()){
            m_enaPromtUser |= FILE_EXISTS;
            //qDebug( ) << " file exists";
        }
    }

    return (Source.getType() == Z88_DevView::type_File) &&
            ((m_enaPromtUser & PROMPT_USER) ||
            ((m_enaPromtUser & FILE_EXISTS) &&
             !(m_enaPromtUser & (NO_TO_OW_ALL | YES_TO_OW_ALL))) );
}

bool Z88Comms::CRLF_TranslationEnable(bool ena)
{
    if (ena) {
        cmdStatus("Sending Enable CRLF Translation");
        if(!m_sport.linefeedConvOn()){
            emit boolCmd_result("CRLF Translation ON", false);
            return false;
        }
    } else {
        cmdStatus("Sending Disable CRLF Translation");
        if(!m_sport.linefeedConvOff()){
            emit boolCmd_result("CRLF Translation OFF", false);
            return false;
        }
    }

    return true;
}

bool Z88Comms::BYTE_TranslationEnable(bool ena)
{
    if (ena){
        cmdStatus("Sending Enable Byte Translation");
        if(!m_sport.translationOn()){
            emit boolCmd_result("Byte Translation ON", false);
            return false;
        }
    } else {
        cmdStatus("Sending Disable Byte Translation");
        if(!m_sport.translationOff()){
            emit boolCmd_result("Byte Translation OFF", false);
            return false;
        }
    }
    return true;
}

/**
 * @brief Validate if host file is a binary file (file must exist)
 * @param hostfilename
 * @return true if file contains bytes < Ascii value 32 (except TAB, LF & CR)
 */
bool Z88Comms::isBinaryFile(QString hostFilename)
{
    QFile hostFile(hostFilename);
    QByteArray fileContents;
    bool fileIsBinary = false;

    if (hostFile.exists() == true) {
        if (hostFile.open(QIODevice::ReadOnly) == true) {
            // file opened for read-only...
            fileContents = hostFile.readAll();
            hostFile.close();

            for (int b=0; b<fileContents.length(); b++) {
                if (((fileContents.at(b) < 9) || (fileContents.at(b) > 13)) && (fileContents.at(b) < 32)) {
                    fileIsBinary = true;
                    break;
                }
            }
        }
    }

    return fileIsBinary;
}

/**
 * @brief Get CRC-32 of specified hostfile
 * @param hostfilename
 * @return CRC-32 checksum (0xFFFFFFFF if file doesn't exist)
 */
uint Z88Comms::hostFileCrc32(QString hostFilename)
{
    QFile hostFile(hostFilename);
    uint checksum = 0xffffffff;

    if (hostFile.exists() == true) {
        if (hostFile.open(QIODevice::ReadOnly) == true) {
            // file opened for read-only...
            QByteArray fileContents = hostFile.readAll();
            hostFile.close();

            Crc32 crc32 = Crc32().checksum(fileContents);
            checksum = crc32.getChecksum();
        }
    }

    return checksum;
}

/**
 * @brief Get size of specified hostfile
 * @param hostfilename
 * @return size of file in bytes
 */
uint Z88Comms::hostFileFileSize(QString hostFilename)
{
    QFile hostFile(hostFilename);
    uint fileSize = 0;

    if (hostFile.exists() == true) {
        if (hostFile.open(QIODevice::ReadOnly) == true) {
            // file opened for read-only...
            fileSize = hostFile.size();
            hostFile.close();
        }
    }

    return fileSize;
}

/**
 * @brief Compare equal filesize, then retrieve CRC-32 of remote Z88 file
 * @param z88Filename
 * @param z88FileSize (default 0)
 * @param hostFileSize (default 0)
 * @return 32bit CRC32, if retrieved, otherwise 0xffffffff when failure
 */
uint Z88Comms::z88FileCrc32(QString &z88Filename, int z88FileSize, int hostFileSize)
{
    bool status;
    QString msg = "Receiving CRC-32 of " + z88Filename;

    cmdStatus(msg);
    if (z88FileSize == 0)
        // Don't know the Z88 file size, retrieve it..
        z88FileSize = m_sport.getFileSize(z88Filename).toInt();

    if ( (hostFileSize > 0) && (z88FileSize != hostFileSize)) {
        // only compare file sizes, if hostfile size is specified, otherwise ignore..
        return 0; // indicate file sizes are not the same
    } else {
        QByteArray fileCrc32String = m_sport.getFileCrc32(z88Filename, z88FileSize);

        if (fileCrc32String.length() == 0){
            emit boolCmd_result(msg, false);
            return 0xffffffff;
        } else {
            return fileCrc32String.toUInt(&status,16);
        }
    }
}
