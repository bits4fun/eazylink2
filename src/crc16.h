/*********************************************************************************************

 EazyLink2 - Fast Client/Server Z88 File Management
 Copyright (C) 2012-2015 Gunther Strube (gstrube@gmail.com) & Oscar Ernohazy

 EazyLink2 is free software; you can redistribute it and/or modify it under the terms of the
 GNU General Public License as published by the Free Software Foundation;
 either version 2, or (at your option) any later version.
 EazyLink2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.
 You should have received a copy of the GNU General Public License along with EazyLink2;
 see the file COPYING. If not, write to the
 Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

**********************************************************************************************/

#ifndef CRC16_H
#define CRC16_H

#include <QtGlobal>
#include <QtCore/QByteArray>

class Crc16
{
private:
    quint16 crc16Val;
public:
    Crc16();
    Crc16(quint16 initValue);
    void resetChecksum();
    void resetChecksum(quint16 initValue);
    quint16 getChecksum();
    quint16 checksum(const QByteArray &s);
    quint16 checksum(const QByteArray &s, int offset, int len);
};

#endif // CRC16_H
