
------------------------------------------------------------------------------------
Introduction
------------------------------------------------------------------------------------

26th August 2015

Eazylink2 is a remote file management tool for Cambridge Z88 portable, using RS-232
cable or USB serial adapters.

This desktop application is designed to work optimally using the Z88 Eazylink
Popdown V5.2.3 (for all OZ ROM's) or later. This popdown must be installed on an
application card, inserted into one of the external slots of your Cambridge Z88.

We have already provided ready-made executables for Windows, Mac, Raspberry Pi and
Ubuntu 15.04, ready to be downloaded here:

http://sourceforge.net/projects/z88/files/EazyLink%20Desktop%20Client/

- The application source code

For those who use other platforms which we have not provided executables, it is
necessary to compile the application yourself.

This is very easy (see instructions below).

EazyLink2 is a pure Qt5 application and only requires the Qt5 SDK to be installed
on your desktop system.


------------------------------------------------------------------------------------
Cambridge Z88 Eazylink popdown
------------------------------------------------------------------------------------

EazyLink2 desktop client talks to the Eazylink popdown on the Cambridge Z88. Copy
this link into your browser to download a free copy and follow instructions on
how to install the popdown:

https://cambridgez88.jira.com/wiki/x/HICvAw

This desktop application is also designed to work optimally with Eazylink popdown as
part of Cambridge Z88 OZ v4.5 ROM or later, enabling 38.400 BPS transfer speeds.

OZ v4.5 is typically installed on an application card, inserted into
slot 1 of your Cambridge Z88.

The Eazylink2 Desktop Client is also designed to transfer files to your Z88 using
the built-in Imp/Export popdown, in case you don't yet have the Eazylink popdown
installed on your Z88.



------------------------------------------------------------------------------------
Compile Eazylink2 from source packages
------------------------------------------------------------------------------------

In order to compile the sources of the EazyLink2 desktop client (you have them,
since you're reading this text), simply download and install a QT 5.4 SDK from
this link:

http://download.qt.io/archive/qt/5.4/5.4.2/

Start QT Creator application and load the eazylink2.pro project. Define a compile
targets (Debug and/or Release) then build the application.

Read here how to deploy the QT5 application (platform-specific):

http://doc.qt.io/qt-5/deployment.html

------------------------------------------------------------------------------------
Deploying Eazylink2 on windows
------------------------------------------------------------------------------------

Qt provides a deployment tool for Windows: windeployqt. You will need to perform this step to generate all the dependent resources.

On the command line:

set PATH=%PATH%;<qt_install_prefix>/bin
windeployqt --dir /path/to/deployment/dir /path/to/qt/application.exe


For Example:

set PATH=%PATH%;c:\Qt\6.5.2\mingw_64\bin\
windeployqt --dir c:\Users\steven\Development\z88\release c:\Users\steven\Development\z88\build-eazylink2-Desktop_Qt_6_5_2_MinGW_64_bit-Release\bin\eazylink2.exe

Once done it's just a matter to copy the eazylink2.exe file over to the release folder.

copy c:\Users\steven\Development\z88\build-eazylink2-Desktop_Qt_6_5_2_MinGW_64_bit-Release\bin\eazylink2.exe c:\Users\steven\Development\z88\release

Everything will now run from this single directory.  It can be zipped or wrapped in an installer.

------------------------------------------------------------------------------------
IMPORTANT for Linux (access rights to serial port)
------------------------------------------------------------------------------------

On Linux, all serial port devices are owned by the "dialout" system group.
That means the login-user who runs the EazyLink client must be part of this group
in order to be able to open the serial ports.

Some Linux distributions have the "dialout" group pre-registered for new login-users
which enables serial port access out-of-the-box.. Others not.

The Installer is executed by the login-user and doesn't require root (administrator)
access. This is to have the sugar-coating of desktop application icons and easy
un-install. However it doesn't manage the dialout group.

If you get "Could not open serial port /dev/XXXX", do as follows in a command shell
(bash or similar) as root:

On Ubuntu:
sudo adduser <your-login-user> dialout

Other Linux systems:
su<ENTER>
{enter root password}
(as root) adduser <your-login-user> dialout



------------------------------------------------------------------------------------
Helping out by registering as user in our Open Source project
------------------------------------------------------------------------------------

Please, sign up with an account on our project (all our work
is open source), and provide any feedback you may have
(ideas, bug reports, etc):

https://cambridgez88.jira.com/wiki/x/BABN
(link to wiki page of EazyLink Client)

or

https://cambridgez88.jira.com/wiki/display/welcome/Cambridge+Z88
(to see our general news about the Z88 project)
