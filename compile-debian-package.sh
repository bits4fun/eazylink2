#!/bin/bash

# *************************************************************************************
#
# EazyLink2 - Fast Client/Server Z88 File Management
# Copyright (C) 2012-2015 Gunther Strube (gstrube@gmail.com) & Oscar Ernohazy
#
# ---------------------------------------------------------------------------------------
#
# Build script to generate Ubuntu/Debian offline source + binary package for Eazylink2.
#
# Build requirements (install these packages before running this compile script):
# Requirements:
# 1) Debian / Ubuntu package development tools
#    build-essential fakeroot dpkg-dev devscripts dh-make
# 2) libqt5serialport5-dev, qtbase5-dev, qtbase5-dev-tools, qt5-qmake, qt5-default (>= v5.2.1)
#    make / gcc compiler & debian package build tools
#
# import GPG keys (optional):
#	gpg --import mygpgkey_pub.asc
#	gpg --allow-secret-key-import --import mygpgkey-secret.asc	
#
# *************************************************************************************

# also remember to change version in debian/changelog
EAZYLINK2_PACKAGEVERSION=1.0.3~ppa2

EAZYLINK2_PATH=$PWD

command -v debuild >/dev/null 2>&1 || { echo "debuild utility is not available on system!" >&2; exit 1; }
command -v dpkg-buildpackage >/dev/null 2>&1 || { echo "dpkg-buildpackage utility is not available on system!" >&2; exit 1; }
command -v tar >/dev/null 2>&1 || { echo "tar utility is not available on system!" >&2; exit 1; }
command -v qmake >/dev/null 2>&1 || { echo "qmake utility is not available on system!" >&2; exit 1; }
command -v make >/dev/null 2>&1 || { echo "make utility is not available on system!" >&2; exit 1; }
command -v g++ >/dev/null 2>&1 || { echo "GNU C++ compiler is not available on system!" >&2; exit 1; }

# delete any previous build output
rm -fR eazylink2-*
rm -f eazylink2_*
rm -f *.o
rm -fR bin
rm -fR build
rm -fR Makefile
rm -f eazylink2.pro.user
rm -fR *~

# generate original package, which is used to build package
tar --exclude ".*" -czf eazylink2_$EAZYLINK2_PACKAGEVERSION.orig.tar.gz *
if test $? -gt 0; then
	echo "Unable to create eazylink2_$EAZYLINK2_PACKAGEVERSION.orig.tar.gz archive. build-script aborted."
	exit 1
fi

mkdir eazylink2-$EAZYLINK2_PACKAGEVERSION
if test $? -gt 0; then
	echo "Unable to create eazylink2-$EAZYLINK2_PACKAGEVERSION folder. build-script aborted."
	exit 1
fi

# extract archive into build folder
tar xzf eazylink2_$EAZYLINK2_PACKAGEVERSION.orig.tar.gz -C eazylink2-$EAZYLINK2_PACKAGEVERSION

# build the DEB source package...
cd eazylink2-$EAZYLINK2_PACKAGEVERSION
debuild -i -I -S -sa

if test $? -eq 0; then
	echo "DEB source package built successfully."
fi

# build the debian architecture-specific installer package
dpkg-buildpackage -rfakeroot -us -uc -b

if test $? -eq 0; then
	echo "DEB installer package built successfully."
fi

cd $MPM_PATH
