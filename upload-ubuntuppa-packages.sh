#!/bin/bash

# *************************************************************************************
#
# EazyLink2 - Fast Client/Server Z88 File Management
# Copyright (C) 2012-2015 Gunther Strube (gstrube@gmail.com) & Oscar Ernohazy
#
# Build script to generate Debian source packages of Eazylink2, uploaded to ppa:cambridgez88/eazylink2
#
# EazyLink2 is free software; you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation;
# either version 2, or (at your option) any later version.
# EazyLink2 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with EazyLink2;
# see the file COPYING. If not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# Requirements:
# 1) Debian / Ubuntu package development tools
#    build-essential fakeroot dpkg-dev devscripts dh-make
# 2) libqt5serialport5-dev, qtbase5-dev, qtbase5-dev-tools, qt5-qmake, qt5-default (>= v5.2.1)
#    make / gcc compiler & debian package build tools
#
# *************************************************************************************

# also remember to change version in debian/changelog
EAZYLINK2_PACKAGEVERSION=1.0.3~ppa2

# get the relative path to this script
EAZYLINK2_PATH=$PWD

command -v debuild >/dev/null 2>&1 || { echo "debuild utility is not available on system!" >&2; exit 1; }
command -v dpkg-buildpackage >/dev/null 2>&1 || { echo "dpkg-buildpackage utility is not available on system!" >&2; exit 1; }
command -v tar >/dev/null 2>&1 || { echo "tar utility is not available on system!" >&2; exit 1; }
command -v qmake >/dev/null 2>&1 || { echo "qmake utility is not available on system!" >&2; exit 1; }
command -v make >/dev/null 2>&1 || { echo "make utility is not available on system!" >&2; exit 1; }
command -v g++ >/dev/null 2>&1 || { echo "GNU C++ compiler is not available on system!" >&2; exit 1; }

# delete any previous build output
rm -fR eazylink2-*
rm -f eazylink2_*
rm -f *.o
rm -fR bin
rm -fR build
rm -fR Makefile
rm -f eazylink2.pro.user
rm -fR *~

# generate original package, which is used to build package
tar --exclude ".*" -czf eazylink2_$EAZYLINK2_PACKAGEVERSION.orig.tar.gz *
if test $? -gt 0; then
	echo "Unable to create eazylink2_$EAZYLINK2_PACKAGEVERSION.orig.tar.gz archive. build-script aborted."
	exit 1
fi

mkdir eazylink2-$EAZYLINK2_PACKAGEVERSION
if test $? -gt 0; then
	echo "Unable to create eazylink2-$EAZYLINK2_PACKAGEVERSION folder. build-script aborted."
	exit 1
fi

# extract archive into build folder
tar xzf eazylink2_$EAZYLINK2_PACKAGEVERSION.orig.tar.gz -C eazylink2-$EAZYLINK2_PACKAGEVERSION
# the ubuntu source package builder will create ubuntu-release specific orig.tar.gz packages
rm -f eazylink2_$EAZYLINK2_PACKAGEVERSION.orig.tar.gz

# build the DEB source package...
cd eazylink2-$EAZYLINK2_PACKAGEVERSION
$EAZYLINK2_PATH/ppa_publish ppa:cambridgez88/eazylink2 trusty vivid wily

if test $? -eq 0; then
	echo "SOURCE DEB package built successfully."
fi
cd $EAZYLINK2_PATH
