26th August 2015

Welcome to EazyLink2 V1.0 on your favorite Windows desktop!

The Z88 Team is very proud to present to you the first
application to transfer files to your beloved Cambridge Z88
that is available on five desktop operating systems
simultaneously.

It is very stable, and have been tested on multiple Linux,
Windows and Mac OSX platforms.

Please, sign up with an account on our project (all our work
is open source), and provide any feedback you may have
(ideas, bug reports, etc):

https://cambridgez88.jira.com/wiki/x/BABN
(link to wiki page of EazyLink Client)

or

https://cambridgez88.jira.com/wiki/display/welcome/Cambridge+Z88
(to see our general news about the Z88 project)

We have developed the EazyLink application using Qt, a cross-
platform application development toolkit, available for
Linux, WindowsXP/7+, Mac OSX and Raspberry Pi2. The Installer
you have been using is Bitrock's InstallerBuilder, also generating
multiplatform application installers.

This desktop application is designed to work optimally using the
Z88 Eazylink Popdown V5.2.3 (for all OZ ROM's) or later.
This popdown must be installed on an application card, inserted into
one of the external slots of your Cambridge Z88.

Copy this link into your browser to download a free copy and
follow instructions:

https://cambridgez88.jira.com/wiki/x/HICvAw

This desktop application is also designed to work optimally with
Eazylink popdown as part of Cambridge Z88 OZ v4.5 ROM or later,
enabling 38.400 BPS transfer speeds.

OZ v4.5 is typically installed on an application card, inserted into
slot 1 of your Cambridge Z88.


The Eazylink2 Desktop Client is also designed to transfer
files to your Z88 using the built-in Imp/Export popdown, in
case you don't yet have the Eazylink popdown installed on
your Z88.

                         -- * O * --

The user guide is available online.

Click on "Help" -> "User Guide", and a browser will load it
automatically.
