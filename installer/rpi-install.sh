#!/bin/bash
EAZDESKTOPICON=$HOME/Desktop/Eazylink2.desktop
EAZINSTALLPATH=$HOME/bin/eazylink2
mkdir -p $EAZINSTALLPATH
cp -f ./eazylink2.png $EAZINSTALLPATH
cp -f ./eazylink2 $EAZINSTALLPATH

rm -f $EAZDESKTOPICON
echo "[Desktop Entry]" >> $EAZDESKTOPICON
echo "Name=Eazylink2" >> $EAZDESKTOPICON
echo "Comment=Remote file management Utility for Cambridge Z88" >> $EAZDESKTOPICON
echo "Icon=$EAZINSTALLPATH/eazylink2.png" >> $EAZDESKTOPICON
echo "Exec=$EAZINSTALLPATH/eazylink2" >> $EAZDESKTOPICON
echo "Type=Application" >> $EAZDESKTOPICON
echo "Encoding=UTF-8" >> $EAZDESKTOPICON
echo "Terminal=false" >> $EAZDESKTOPICON

